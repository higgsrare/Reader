import ROOT as root
from array import *
import fpformat as fp

root.gROOT.SetBatch(1)
root.gStyle.SetOptStat(0)
root.gStyle.SetOptTitle(0)
root.TH1.SetDefaultSumw2(1) 
root.gStyle.SetPalette(1)


histolabel=[	
    "Resolved selection",  
    "H_{T}^{had}>500 GeV",
    "H_{T}^{had}>600 GeV",
    "2 CamKt15 >200 GeV",
    "2 CamKt15 >300 GeV",
    "Lead CamKt15 >300 GeV",
    "CamKt15 sum > 500 GeV",
    "CamKt15 sum > 600 GeV",
    "2 BDRS CamKt15 >200 GeV",
    "2 BDRS CamKt15 >300 GeV",
    "BDRS Lead CamKt15 >300 GeV",
    "BDRS CamKt15 sum > 500 GeV",
    "BDRS CamKt15 sum > 600 GeV",
    ]



BRrw=[0.931,
      0.801,
      0.965,
]

histolist=[
    "cutflow_6j_4b",
    "cutflow_6j_3b",
    "cutflow_5j_4b",
    "cutflow_6j_4b_t",
    "cutflow_6j_3b_t",
    "cutflow_5j_4b_t",
    "cutflow_6j_4b_th",
    "cutflow_6j_3b_th",
    "cutflow_5j_4b_th",
    "cutflow_6j_4b_HTT",
    "cutflow_6j_3b_HTT",
    "cutflow_5j_4b_HTT",
    ]

xsec=23.268 #fb
lumi=20 #fb-1
nlok=1.4036
genevts=1.e6

SF=(xsec*lumi*nlok)/genevts

c1 = root.TCanvas("canvas","",800,600)
leg = root.TLegend(0.5,0.60,0.80,0.93)
leg.SetFillColor(0)
leg.SetLineColor(0)
#pad1 = root.TPad("pad1","pad1",0,0.2,1,1)
#pad1.SetTopMargin(0.05)
#pad1.SetBottomMargin(0.285)
#pad1.SetLogy(1)
#pad1.Draw()
#pad2 = root.TPad("pad2","pad2",0,0,1,0.30)
#pad2.SetTopMargin(0)
#pad2.SetBottomMargin(0.3)
#pad2.Draw()



file = root.TFile.Open("output.root")
leg.Clear()
ratios=[]

for i,h in enumerate(histolist):
    z=0    
    histo=file.Get(h)
    histo.SetDirectory(0)
    histo.Scale(SF)
    print histo.GetName()
    for j in range(1,histo.GetNbinsX()):
        if(histo.GetBinContent(j)>0):
            histo.GetXaxis().SetBinLabel(j,histolabel[j-2])
            print "Eventi aspettati   ",histolabel[j-2], "   ",fp.fix(histo.GetBinContent(j),2)
            #if z==0:
             #   print "Eventi aspettati   ",histolabel[j-2], " RW  ",fp.fix(histo.GetBinContent(j)/BRrw[i],2)
              #  z+=1
    histo.Draw()
    c1.SaveAs(h+".png")
    

