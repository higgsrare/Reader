#include "Reader/TopTruthReweighting.h"


static float ttbarPtUpBins[4]= { 40.e3, 170.e3, 340.e3, 1000.e3 };
static float sequential_topPtUpBins[7]= { 50.e3, 100.e3, 150.e3, 200.e3, 250.e3, 350.e3, 800.e3 };

static float ttbarPt_NOMINAL[4] = { 0.0409119,  -0.0121258,  -0.188448,  -0.316183 };
static float sequential_topPt_NOMINAL[7] = { 0.0139729, 0.0128711, 0.00951743, 0.00422323, -0.0352631, -0.0873852, -0.120025, };
static float ttbarPt_PowHer_NOMINAL[4] = {0.0790586,  -0.0438019,  -0.221803,  -0.370303};
static float sequential_topPt_PowHer_NOMINAL[7] = { -0.0817317, 0.00234056, 0.0295038, 0.0307822, 0.0178486, 0.0155615, -0.00327289 };

static float sequential_topPt_UP_ISRFSR[7] = { -0.0172321, 0.000228524, 0.0190734, 0.0321922, -0.00524551, -0.0606578, -0.0850999, };
static float sequential_topPt_DOWN_ISRFSR[7] = { 0.0434901, 0.029907, -0.00139654, -0.0250363, -0.0660721, -0.113041, -0.151856, };
static float sequential_topPt_UP_Fragmentation[7] = { -0.000428021, 0.0144119, 0.0103852, 0.0101372, -0.0368183, -0.0907707, -0.104522, };
static float sequential_topPt_DOWN_Fragmentation[7] = { 0.0282609, 0.00965357, 0.00707412, 0.00279391, -0.0308473, -0.0803739, -0.131672, };
static float sequential_topPt_UP_MCGenerator[7] = { -0.0015291, 0.0231146, 0.0181124, 0.00602686, -0.0513448, -0.130822, -0.21204, };
static float sequential_topPt_DOWN_MCGenerator[7] = { 0.0249864, 0.00985491, -0.00107294, -0.00232321, -0.0242983, -0.0455976, -0.0281206, };
static float sequential_topPt_UP_JER[7] = { 0.0112931, 0.0127244, 0.00523913, 0.00481522, -0.0269958, -0.0741523, -0.0965351, };
static float sequential_topPt_DOWN_JER[7] = { 0.0142784, 0.0215486, 0.0137571, -0.00352812, -0.0550353, -0.10927, -0.150771, };
static float sequential_topPt_UP_bJES[7] = { -0.00264782, 0.00945258, 0.0191419, 0.0151259, -0.032624, -0.0872676, -0.120662, };
static float sequential_topPt_DOWN_bJES[7] = { 0.0290663, 0.0174549, 0.00029254, -0.00724715, -0.0400869, -0.0884566, -0.119308, };
static float sequential_topPt_UP_closebyJES[7] = { -0.00324571, 0.00872803, 0.0173219, 0.0135056, -0.0274987, -0.0795088, -0.111324, };
static float sequential_topPt_DOWN_closebyJES[7] = { 0.0291626, 0.0185578, 0.00253487, -0.00722212, -0.0461485, -0.0997275, -0.134288, };
static float sequential_topPt_UP_etacalibJES[7] = { 0.00782764, 0.0120407, 0.0137041, 0.00794506, -0.0354335, -0.0924156, -0.130565, };
static float sequential_topPt_DOWN_etacalibJES[7] = { 0.0183909, 0.0148292, 0.00492918, -0.000747144, -0.0332663, -0.0812135, -0.110201, };
static float sequential_topPt_UP_effdetset1JES[7] = { -0.000516593, 0.00798595, 0.0165353, 0.013974, -0.026176, -0.0800426, -0.11356, };
static float sequential_topPt_DOWN_effdetset1JES[7] = { 0.0245768, 0.0196748, 0.00525057, -0.00880069, -0.0479453, -0.0954949, -0.12116, };
static float sequential_topPt_UP_btageff[7] = { 9.4533e-05, 0.00569248, 0.012538, 0.0171587, -0.0152649, -0.0645427, -0.0948508, };
static float sequential_topPt_DOWN_btageff[7] = { 0.02517, 0.0185086, 0.00654054, -0.00655884, -0.0508953, -0.10155, -0.134158, };

static float ttbarPt_UP_ISRFSR[4] = { 0.143962,  -0.130473,  -0.332904,  -0.447613, };
static float ttbarPt_DOWN_ISRFSR[4] = { -0.0621383,  0.106222,  -0.0439914,  -0.184754, };
static float ttbarPt_UP_Fragmentation[4] = { 0.0501761,  -0.0150894,  -0.245743,  -0.335193, };
static float ttbarPt_DOWN_Fragmentation[4] = { 0.0316478,  -0.00916213,  -0.131152,  -0.297173, };
static float ttbarPt_UP_MCGenerator[4] = { -0.00134908,  0.025611,  -0.130584,  -0.329586, };
static float ttbarPt_DOWN_MCGenerator[4] = { 0.083173,  -0.0498626,  -0.246311,  -0.30278, };
static float ttbarPt_UP_JER[4] = { 0.0743252,  -0.0474917,  -0.24412,  -0.369726, };
static float ttbarPt_DOWN_JER[4] = { 0.00749867,  0.0232401,  -0.132775,  -0.26264, };
static float ttbarPt_UP_bJES[4] = { 0.0382056,  -0.0114342,  -0.186175,  -0.314816, };
static float ttbarPt_DOWN_bJES[4] = { 0.0402874,  -0.0112367,  -0.192668,  -0.321312, };
static float ttbarPt_UP_closebyJES[4] = { 0.0367483,  -0.00609972,  -0.177248,  -0.306746, };
static float ttbarPt_DOWN_closebyJES[4] = { 0.0447633,  -0.0186457,  -0.198267,  -0.325073, };
static float ttbarPt_UP_etacalibJES[4] = { 0.0319601,  -0.00382761,  -0.166698,  -0.293754, };
static float ttbarPt_DOWN_etacalibJES[4] = { 0.0544438,  -0.022696,  -0.209061,  -0.339433, };
static float ttbarPt_UP_effdetset1JES[4] = { 0.034146,  -0.00758153,  -0.173109,  -0.297994, };
static float ttbarPt_DOWN_effdetset1JES[4] = { 0.0467411,  -0.0158797,  -0.204679,  -0.337176, };
static float ttbarPt_UP_btageff[4] = { 0.0411201,  -0.0127185,  -0.185445,  -0.309345, };
static float ttbarPt_DOWN_btageff[4] = { 0.0395588,  -0.0107427,  -0.18723,  -0.317209, };

TopTruthReweighting::TopTruthReweighting() {
}

TopTruthReweighting::~TopTruthReweighting() {
}


float TopTruthReweighting::GetDataReweight_PowhegHerwig(float top_pt, float ttbar_pt) {
  return GetTTbarPtWeight_Powheg(ttbar_pt, -999)*GetSequentialTopPtWeight_Powheg(top_pt, -999);
}

float TopTruthReweighting::GetDataReweight_Powheg(float top_pt, float ttbar_pt, int sys) {
  return GetTTbarPtWeight_Powheg(ttbar_pt, sys)*GetSequentialTopPtWeight_Powheg(top_pt, sys);
}

float TopTruthReweighting::GetTTbarPtWeight_Powheg(float pT, int sys){
  int index=3;
  for (unsigned int i=0; i<4; i++) {
    if (pT < ttbarPtUpBins[i]) {
      index=i;
      break;
    }
  }
  switch(sys){
    case  -999: return 1 + ttbarPt_PowHer_NOMINAL[index];
    case  0: return 1 + ttbarPt_NOMINAL[index];
    case  1: return 1 + ttbarPt_UP_ISRFSR[index];
    case -1: return 1 + ttbarPt_DOWN_ISRFSR[index];
    case  2: return 1 + ttbarPt_UP_Fragmentation[index];
    case -2: return 1 + ttbarPt_DOWN_Fragmentation[index];
    case  3: return 1 + ttbarPt_UP_MCGenerator[index];
    case -3: return 1 + ttbarPt_DOWN_MCGenerator[index];
    case  4: return 1 + ttbarPt_UP_JER[index];
    case -4: return 1 + ttbarPt_DOWN_JER[index];
    case  5: return 1 + ttbarPt_UP_bJES[index];
    case -5: return 1 + ttbarPt_DOWN_bJES[index];
    case  6: return 1 + ttbarPt_UP_closebyJES[index];
    case -6: return 1 + ttbarPt_DOWN_closebyJES[index];
    case  7: return 1 + ttbarPt_UP_etacalibJES[index];
    case -7: return 1 + ttbarPt_DOWN_etacalibJES[index];
    case  8: return 1 + ttbarPt_UP_effdetset1JES[index];
    case -8: return 1 + ttbarPt_DOWN_effdetset1JES[index];
    case  9: return 1 + ttbarPt_UP_btageff[index];
    case -9: return 1 + ttbarPt_DOWN_btageff[index];
    default:
      std::cout << "TopTruthReweighting: systematic doesn't exist: " << sys << std::endl;
      exit(1);
  }
}

float TopTruthReweighting::GetSequentialTopPtWeight_Powheg(float pT, int sys) {
  int index=6;
  for (unsigned int i=0; i<7; i++) {
    if (pT < sequential_topPtUpBins[i]) {
      index=i;
      break;
    }
  }
  switch(sys){
    case  -999: return 1 + sequential_topPt_PowHer_NOMINAL[index];
    case  0: return 1 + sequential_topPt_NOMINAL[index];
    case  1: return 1 + sequential_topPt_UP_ISRFSR[index];
    case -1: return 1 + sequential_topPt_DOWN_ISRFSR[index];
    case  2: return 1 + sequential_topPt_UP_Fragmentation[index];
    case -2: return 1 + sequential_topPt_DOWN_Fragmentation[index];
    case  3: return 1 + sequential_topPt_UP_MCGenerator[index];
    case -3: return 1 + sequential_topPt_DOWN_MCGenerator[index];
    case  4: return 1 + sequential_topPt_UP_JER[index];
    case -4: return 1 + sequential_topPt_DOWN_JER[index];
    case  5: return 1 + sequential_topPt_UP_bJES[index];
    case -5: return 1 + sequential_topPt_DOWN_bJES[index];
    case  6: return 1 + sequential_topPt_UP_closebyJES[index];
    case -6: return 1 + sequential_topPt_DOWN_closebyJES[index];
    case  7: return 1 + sequential_topPt_UP_etacalibJES[index];
    case -7: return 1 + sequential_topPt_DOWN_etacalibJES[index];
    case  8: return 1 + sequential_topPt_UP_effdetset1JES[index];
    case -8: return 1 + sequential_topPt_DOWN_effdetset1JES[index];
    case  9: return 1 + sequential_topPt_UP_btageff[index];
    case -9: return 1 + sequential_topPt_DOWN_btageff[index];
    default:
      std::cout << "TopTruthReweighting: systematic doesn't exist: " << sys << std::endl;
      exit(1);
  }
}
  
