#include "Reader/chi2.h"
#include <iostream>

#define WHM 80.399
#define TOPMASS 172.5
#define SWMQ 100.
#define STMQ 400.


using namespace std;

std::vector<double> lepnu(TLorentzVector& l, TLorentzVector& nu,vector<int> &info) {

  std::vector<double> pznu;
  TLorentzVector w;
  const double WMass = 80.399;

  double nu_Px=nu.Pt()*TMath::Cos(nu.Phi());
  double nu_Py=nu.Pt()*TMath::Sin(nu.Phi());

  double MissingPt = sqrt( nu_Px*nu_Px+nu_Py*nu_Py );
  
  double alpha  = 0.5*(WMass*WMass-l.M()*l.M());
  double beta   = alpha+nu_Px*l.Px()+nu_Py*l.Py();
  double gamma  = -(beta*beta-(l.E()*l.E()*MissingPt*MissingPt))/(l.E()*l.E()-l.Pz()*l.Pz());
  double lambda = 2.*beta*l.Pz()/(l.E()*l.E()-l.Pz()*l.Pz());
  
  double delta  = lambda*lambda-4.*gamma;
  int no_sol=0,two_sol=0;
  
  if(delta < 0) { 
    nu.SetPtEtaPhiM(nu.Pt(),l.Eta(),nu.Phi(), 0.); pznu.push_back(nu.Pz()); return pznu;
    no_sol=1;
  }
  else{
    two_sol=1;
  }
  info.push_back(no_sol);
  info.push_back(two_sol);
    
  double Pz1 = (lambda-sqrt(delta))/2.;
  double Pz2 = (lambda+sqrt(delta))/2.;

  //first neutrino is the one with lowest pz
  if (TMath::Abs(Pz1)<TMath::Abs(Pz2)) {
    pznu.push_back(Pz1);   pznu.push_back(Pz2);
  }
  else {
    pznu.push_back(Pz2);   pznu.push_back(Pz1);
  }
  return pznu;
}

double calcChi2(TLorentzVector& wh, TLorentzVector& th, TLorentzVector& tlep) {
  
  double wm   = wh.M();
  double lm   = tlep.M();
  double tm   = th.M();
  
  return ( ((wm-WHM)*(wm-WHM))/SWMQ + ((tm-TOPMASS)*(tm-TOPMASS))/STMQ + ((lm-TOPMASS)*(lm-TOPMASS))/STMQ );//
}


double DoChi2SemiLep(TLorentzVector& j1,
		     TLorentzVector& j2,
		     TLorentzVector& j3,
		     TLorentzVector& j4,
		     TLorentzVector& l,
		     TLorentzVector& nu,
		     TLorentzVector& hadronic_top,
		     TLorentzVector& leptonic_top,
		     TLorentzVector& hadronic_w,
		     TLorentzVector& leptonic_w,
		     vector<int> &info){

  vector<TLorentzVector> jets;
  jets.push_back(j1);
  jets.push_back(j2);
  jets.push_back(j3);
  jets.push_back(j4);
  vector <TLorentzVector>::iterator it = jets.begin();
  std::vector<double> pznu = lepnu(l,nu,info);
  
  double minchi2=1.e300; //huge number to be sure that there is always 1 solution.
  TLorentzVector mintlep, minwh, minth,minwl;
  
  int nus=pznu.size();
  //int limit = std::min(nus,1);
  for (int nn=0; nn<nus; nn++) {
    nu.SetPxPyPzE(nu.Px(),nu.Py(),pznu.at(nn),
		  sqrt(nu.Perp2() + pznu.at(nn)*pznu.at(nn)));
    TLorentzVector wl = l+nu;
  
    int jhigh_limit = jets.size();
    
    for (int jj=0; jj<jhigh_limit;jj++) {
      it = jets.begin()+jj;
      TLorentzVector *bjl = &(*it);
      TLorentzVector tlep = wl + *bjl;
      
      for (int jk=0; jk<jhigh_limit;jk++) {
	if (jk==jj) continue;
	it = jets.begin()+jk;
	TLorentzVector *jh1 = &(*it);	
	TLorentzVector wh;
	  
	for (int jl=0; jl<jhigh_limit;jl++) {
	  if ((jl==jj)||(jl==jk)) continue;
	  it = jets.begin()+jl;
	  TLorentzVector *jh2 = &(*it);
	      
	  for (int jm=jl+1; jm<jhigh_limit;jm++) {
	    if ((jm==jj)||(jm==jk)) continue;
	  //for(int jm=0;jm<jhigh_limit;jm++){
	  //if ((jm==jj)||(jm==jk)||(jm==jl)) continue;
	    it = jets.begin()+jm;
	    TLorentzVector *jh3 = &(*it);

// 	    if(n_jets-get_nbtaggedjets()>1)
// 	      if ( get_jet_isTagged(jl) || get_jet_isTagged(jm) ) continue;
	    
	    wh = *jh3+*jh2;

	    TLorentzVector  th = wh+*jh1;
	          
	    double tmp_chi2 = calcChi2(wh,th,tlep);
	    if (tmp_chi2<minchi2) {
	      minchi2=tmp_chi2;
	      minth = th;    
	      mintlep = tlep;     
	      minwh = wh;
	      minwl=wl;
	      
	    }
	  }
	}
 	
      } //loop b had jet2
    }//loop b lep jet1
  }//loop over nu solutions
     
  hadronic_top = minth;
  leptonic_top = mintlep;
  hadronic_w = minwh;
  leptonic_w = minwl;
  return minchi2;

}





double DoChi2SemiLepMod(vector<TLorentzVector> &jets,
		     TLorentzVector& l,
		     TLorentzVector& nu,
		     TLorentzVector& hadronic_top,
		     TLorentzVector& leptonic_top,
		     TLorentzVector& hadronic_w,
		     TLorentzVector& leptonic_w,
		     vector<int> &info){

  vector <TLorentzVector>::iterator it = jets.begin();
  std::vector<double> pznu = lepnu(l,nu,info);
  
  double minchi2=1.e300; //huge number to be sure that there is always 1 solution.
  TLorentzVector mintlep, minwh, minth,minwl;
  int lep_j=-1;
  int nus=pznu.size();
  //int limit = std::min(nus,1);
  



  for (int nn=0; nn<nus; nn++) {
    nu.SetPxPyPzE(nu.Px(),nu.Py(),pznu.at(nn),
		  sqrt(nu.Perp2() + pznu.at(nn)*pznu.at(nn)));
    TLorentzVector wl = l+nu;
  
    
    int jhigh_limit = jets.size();
    
    for (int jj=0; jj<jhigh_limit;jj++) {
      it = jets.begin()+jj;
      TLorentzVector *bjl = &(*it);
      TLorentzVector tlep = wl + *bjl;
      
      for (int jk=0; jk<jhigh_limit;jk++) {
	if (jk==jj) continue;
	it = jets.begin()+jk;
	TLorentzVector *jh1 = &(*it);	
	TLorentzVector wh;
	  
	for (int jl=0; jl<jhigh_limit;jl++) {
	  if ((jl==jj)||(jl==jk)) continue;
	  it = jets.begin()+jl;
	  TLorentzVector *jh2 = &(*it);
	      
	  //for (int jm=jl+1; jm<jhigh_limit;jm++) {
	  //if ((jm==jj)||(jm==jk)) continue;
	  for(int jm=0;jm<jhigh_limit;jm++){
	    if ((jm==jj)||(jm==jk)||(jm==jl)) continue;
	    it = jets.begin()+jm;
	    TLorentzVector *jh3 = &(*it);

	    	      
	    wh = *jh3+*jh2;
	    
	    TLorentzVector  th = wh+*jh1;
	    
	    double tmp_chi2 = calcChi2(wh,th,tlep);
	    if (tmp_chi2<minchi2) {
	      minchi2=tmp_chi2;
	      minth = th;    
	      mintlep = tlep;     
	      minwh = wh;
	      minwl=wl;
	      lep_j=jj;
	    }
	    
	    
	  }
	}
 	
      } //loop b had jet2
    }//loop b lep jet1
  }//loop over nu solutions
     
  hadronic_top = minth;
  leptonic_top = mintlep;
  hadronic_w = minwh;
  leptonic_w=minwl;
  info.push_back(lep_j);
  
  return minchi2;

}



