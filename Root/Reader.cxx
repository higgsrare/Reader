#define Reader_cxx
#include "Reader/Reader.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TSystem.h>
//TRF//
#include "BtaggingTRFandRW/BtagOP.h"
#include "BtaggingTRFandRW/TRFinterface.h"

#include "Reader/chi2.h"

const float GeV = 1000.;
const float TOPMASS= 172.5;

void Reader::Begin()
{
  cout<<"IN BEGIN:"<<endl;
  TH1::SetDefaultSumw2(); 
  m_h1d.clear();
  m_output.clear();
  sample_titles.clear();
  FillSystNames();
  
  for(int sys=0;sys<(int) m_syst->size();sys++){

    if(m_ds_name=="ttbar"){
      m_truthreweighter= new TopTruthReweighting();
      m_ds_num=1;
      
      BookOutput(m_out_folder+"/ttlight"+m_suffix_file+"_"+m_syst->at(sys));
      BookOutput(m_out_folder+"/ttb"+m_suffix_file+"_"+m_syst->at(sys));
      BookOutput(m_out_folder+"/ttbb"+m_suffix_file+"_"+m_syst->at(sys));
      BookOutput(m_out_folder+"/ttB"+m_suffix_file+"_"+m_syst->at(sys));
      BookOutput(m_out_folder+"/ttc"+m_suffix_file+"_"+m_syst->at(sys));
      BookOutput(m_out_folder+"/ttcc"+m_suffix_file+"_"+m_syst->at(sys));
      BookOutput(m_out_folder+"/ttC"+m_suffix_file+"_"+m_syst->at(sys));
      sample_titles.push_back("ttlight_"+m_syst->at(sys));
      sample_titles.push_back("ttb_"+m_syst->at(sys));
      sample_titles.push_back("ttbb_"+m_syst->at(sys));
      sample_titles.push_back("ttB_"+m_syst->at(sys));
      sample_titles.push_back("ttc_"+m_syst->at(sys));
      sample_titles.push_back("ttcc_"+m_syst->at(sys));
      sample_titles.push_back("ttC_"+m_syst->at(sys));
    }
    else{
      m_ds_num=2;
     
      BookOutput(m_out_folder+"/ttH"+m_suffix_file+"_"+m_syst->at(sys));
      sample_titles.push_back("ttH_"+m_syst->at(sys));
    }
    for(int i=0;i<(int) sample_titles.size();i++){




      BookTH1D("cutflow_pheno"+sample_titles.at(i),20,0,20);
     
      BookTH1D("hist_nbtag_TRF"+sample_titles.at(i),20,0,20);
      BookTH1D("hist_nbtag_BDRS_TRF"+sample_titles.at(i),20,0,20);
      BookTH1D("hist_nbmicrojets_TRF"+sample_titles.at(i),20,0,20);
      BookTH1D("hist_leadBDRS_m_TRF"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_pt_TRF"+sample_titles.at(i),100,0,500);
      BookTH1D("hist_nBDRS_TRF"+sample_titles.at(i),20,0,20);
      BookTH1D("hist_njets_BDRS_TRF"+sample_titles.at(i),20,0,20);
      
      BookTH1D("hist_higgs_m"+sample_titles.at(i),100,0,500);
      BookTH1D("hist_nbtag"+sample_titles.at(i),20,0,20);
      BookTH1D("hist_njets"+sample_titles.at(i),20,0,20);
      BookTH1D("hist_nmicrojets"+sample_titles.at(i),20,0,20);
      BookTH1D("hist_nbeventtag"+sample_titles.at(i),20,0,20);
      BookTH1D("hist_nfatjet"+sample_titles.at(i),20,0,20);
      BookTH1D("hist_leadjet_pt"+sample_titles.at(i),100,0,750);
      BookTH1D("hist_Amicrojet_pt"+sample_titles.at(i),100,0,300);
      BookTH1D("hist_DRmujet_fatjet"+sample_titles.at(i),100,0,5);
      
      
      ////Optimization plots
      


      const Int_t Nbins = 30;
      Double_t edges[Nbins+1]={0.,2.5,5.,7.5,10.,12.5,15.,17.5,20.,22.5,25.,27.5,30.,32.5,35.,37.5,40.,42.5,45.,47.5,50.,55.,60.,65.,70.,75.,80.,85.,90.,95.,100.};
    
      //const Int_t Nbins_60 = 28;
      // Double_t edges_60[Nbins_60+1]={0.,10.,20.,30.,40.,45.,50.,52.,54.,56.,58.,60.,62.,64.,66.,71.,76.,81.,91.,101.,111.,121.,131.,141.,151.,161.,171.,181.,201.};


      BookTH1D("hist_leadBDRS_R6_m_60GeV"+sample_titles.at(i),Nbins,edges);
      BookTH1D("hist_leadBDRS_R6_pt_60GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R6_m_70GeV"+sample_titles.at(i),100,0,100);
      BookTH1D("hist_leadBDRS_R6_pt_70GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R6_m_80GeV"+sample_titles.at(i),100,0,100);
      BookTH1D("hist_leadBDRS_R6_pt_80GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R6_m_100GeV"+sample_titles.at(i),Nbins,edges);
      BookTH1D("hist_leadBDRS_R6_pt_100GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R6_m_120GeV"+sample_titles.at(i),Nbins,edges);
      BookTH1D("hist_leadBDRS_R6_pt_120GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R6_m_150GeV"+sample_titles.at(i),100,0,100);
      BookTH1D("hist_leadBDRS_R6_pt_150GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R6_m_200GeV"+sample_titles.at(i),Nbins,edges);
      BookTH1D("hist_leadBDRS_R6_pt_200GeV"+sample_titles.at(i),200,0,500);
      
      BookTH1D("hist_leadBDRS_R8_m_50GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R8_pt_50GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R8_m_60GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R8_pt_60GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R8_m_80GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R8_pt_80GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R8_m_100GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R8_pt_100GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R8_m_120GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R8_pt_120GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R8_m_150GeV"+sample_titles.at(i),50,0,200);
      BookTH1D("hist_leadBDRS_R8_pt_150GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R8_m_200GeV"+sample_titles.at(i),50,0,200);
      BookTH1D("hist_leadBDRS_R8_pt_200GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R8_m_250GeV"+sample_titles.at(i),50,0,200);
      BookTH1D("hist_leadBDRS_R8_pt_250GeV"+sample_titles.at(i),200,0,500);






      BookTH1D("hist_leadBDRS_R6_60GeV_d12"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R6_60GeV_d23"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R6_60GeV_t21"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R6_60GeV_t32"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R6_100GeV_d12"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R6_100GeV_d23"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R6_100GeV_t21"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R6_100GeV_t32"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R6_120GeV_d12"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R6_120GeV_d23"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R6_120GeV_t21"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R6_120GeV_t32"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R6_200GeV_d12"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R6_200GeV_d23"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R6_200GeV_t21"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R6_200GeV_t32"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R8_150GeV_d12"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R8_150GeV_d23"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R8_150GeV_t21"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R8_150GeV_t32"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R8_200GeV_d12"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R8_200GeV_d23"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R8_200GeV_t21"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R8_200GeV_t32"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R8_250GeV_d12"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R8_250GeV_d23"+sample_titles.at(i),100,0,50);
      BookTH1D("hist_leadBDRS_R8_250GeV_t21"+sample_titles.at(i),100,0,1);
      BookTH1D("hist_leadBDRS_R8_250GeV_t32"+sample_titles.at(i),100,0,1);


      BookTH1D("hist_leadBDRS_R6_60GeV_nj"+sample_titles.at(i),10,0,10);
      BookTH1D("hist_leadBDRS_R6_100GeV_nj"+sample_titles.at(i),10,0,10);
      BookTH1D("hist_leadBDRS_R6_120GeV_nj"+sample_titles.at(i),10,0,10);
      BookTH1D("hist_leadBDRS_R6_200GeV_nj"+sample_titles.at(i),10,0,10);
      BookTH1D("hist_leadBDRS_R8_150GeV_nj"+sample_titles.at(i),10,0,10);
      BookTH1D("hist_leadBDRS_R8_200GeV_nj"+sample_titles.at(i),10,0,10);
      BookTH1D("hist_leadBDRS_R8_250GeV_nj"+sample_titles.at(i),10,0,10);

      BookTH1D("hist_leadBDRS_R6_60GeV_chi2"+sample_titles.at(i),100,0,100);
      BookTH1D("hist_leadBDRS_R6_100GeV_chi2"+sample_titles.at(i),100,0,100);
      BookTH1D("hist_leadBDRS_R6_120GeV_chi2"+sample_titles.at(i),100,0,100);
      BookTH1D("hist_leadBDRS_R6_200GeV_chi2"+sample_titles.at(i),100,0,100);
      BookTH1D("hist_leadBDRS_R8_150GeV_chi2"+sample_titles.at(i),100,0,100);
      BookTH1D("hist_leadBDRS_R8_200GeV_chi2"+sample_titles.at(i),100,0,100);
      BookTH1D("hist_leadBDRS_R8_250GeV_chi2"+sample_titles.at(i),100,0,100);

      BookTH1D("hist_leadBDRS_R6_60GeV_mw"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R6_100GeV_mw"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R6_120GeV_mw"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R6_200GeV_mw"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R8_150GeV_mw"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R8_200GeV_mw"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R8_250GeV_mw"+sample_titles.at(i),100,0,200);

      BookTH1D("hist_leadBDRS_R6_60GeV_mt"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R6_100GeV_mt"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R6_120GeV_mt"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R6_200GeV_mt"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R8_150GeV_mt"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R8_200GeV_mt"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R8_250GeV_mt"+sample_titles.at(i),100,0,250);

      BookTH1D("hist_leadBDRS_R6_60GeV_diff"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R6_100GeV_diff"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R6_120GeV_diff"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R6_200GeV_diff"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R8_150GeV_diff"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R8_200GeV_diff"+sample_titles.at(i),100,0,200);
      BookTH1D("hist_leadBDRS_R8_250GeV_diff"+sample_titles.at(i),100,0,200);

      BookTH1D("hist_leadBDRS_R6_60GeV_ml"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R6_100GeV_ml"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R6_120GeV_ml"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R6_200GeV_ml"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R8_150GeV_ml"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R8_200GeV_ml"+sample_titles.at(i),100,0,250);
      BookTH1D("hist_leadBDRS_R8_250GeV_ml"+sample_titles.at(i),100,0,250);

  
      /*
      BookTH1D("hist_leadBDRS_R10_m_40GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R10_pt_40GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R10_m_50GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R10_pt_50GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R10_m_60GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R10_pt_60GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R10_m_80GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R10_pt_80GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R10_m_100GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R10_pt_100GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R10_m_120GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R10_pt_120GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R10_m_150GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R10_pt_150GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R10_m_200GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R10_pt_200GeV"+sample_titles.at(i),200,0,500);

      BookTH1D("hist_leadBDRS_R4_m_100GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R4_pt_100GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R4_m_120GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R4_pt_120GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R4_m_150GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R4_pt_150GeV"+sample_titles.at(i),200,0,500);
      BookTH1D("hist_leadBDRS_R4_m_200GeV"+sample_titles.at(i),200,0,200);
      BookTH1D("hist_leadBDRS_R4_pt_200GeV"+sample_titles.at(i),200,0,500);

      
      
	hist_2d_HT_higgs=new TH2F("hist_2d_HT_higgs","",100,0,300,3,0,3);
	hist_2d_HT_top=new TH2F("hist_2d_HT_top"+sample_titles.at(i),"",100,0,1000,100,0,500);
	hist_2d_ung_fatjet_higgs=new TH2F("hist_2d_ung_fatjet_higgs"+sample_titles.at(i),"",100,0,1000,100,0,500);
	hist_2d_ung_fatjet_top=new TH2F("hist_2d_ung_fatjet_top"+sample_titles.at(i),"",100,0,1000,100,0,500);
	hist_2d_ung_fatjetsum_higgs=new TH2F("hist_2d_ung_fatjetsum_higgs"+sample_titles.at(i),"",100,0,1000,100,0,500);
	hist_2d_ung_fatjetsum_top=new TH2F("hist_2d_ung_fatjetsum_top"+sample_titles.at(i),"",100,0,1000,100,0,500);
	hist_2d_bdrs_fatjet_higgs=new TH2F("hist_2d_bdrs_fatjet_higgs"+sample_titles.at(i),"",100,0,1000,100,0,500);
	hist_2d_bdrs_fatjet_top=new TH2F("hist_2d_bdrs_fatjet_top"+sample_titles.at(i),"",100,0,1000,100,0,500);
	hist_2d_bdrs_fatjetsum_higgs=new TH2F("hist_2d_bdrs_fatjetsum_higgs"+sample_titles.at(i),"",100,0,1000,100,0,500);
	hist_2d_bdrs_fatjetsum_top=new TH2F("hist_2d_bdrs_fatjetsum_top"+sample_titles.at(i),"",100,0,1000,100,0,500);
      */
      



    }
    
  }
 
  FillHistoNames();
  
  m_pt_mass=-1;
  if(m_mass==20){m_pt_mass=60.;};
  if(m_mass==30){m_pt_mass=100.;};
  if(m_mass==40){m_pt_mass=120.;};
  if(m_mass==60){m_pt_mass=150.;};
  if(m_mass==80){m_pt_mass=200.;};
  cout<<"MASS =  "<<m_mass<<"  PT CHOSEN = "<<m_pt_mass<<endl;

}

void Reader::Loop()
{

  if (fChain == 0) return;
  
  Long64_t nentries = fChain->GetEntries();
  cout<<"Entries = "<<nentries<<endl;
  if(m_fin_num==-1){m_fin_num=nentries;};
  cout<<"Loop will run over = "<<m_fin_num-m_ini_num<<endl;
  ///////TRF INTERFACE////////
  string pathToConf = gSystem->Getenv("ROOTCOREBIN");
  if(pathToConf == "") cout << "Issue in the ROOTCORBIN path" << endl;
  cout << "pathToConf = " << pathToConf << "  full path = " << pathToConf+"/data/BtaggingTRFandRW/calibConfig.txt" << endl;
  TRFinterface cbc(pathToConf+"/data/BtaggingTRFandRW/calibConfig.txt",0.7892,"AntiKt4TopoLCJVF0_5");
  cbc.setTestMode(1);
  /////////////////////////////
  for(int sys=0;sys<(int) m_syst->size();sys++){
    for (Long64_t jentry=m_ini_num; jentry<m_fin_num;jentry++) {
      fChain->GetEntry(jentry);
      
      
      ///////TRF STUFF///////
      vector<double> jpt,jeta,jMV1;
      vector<int> jpdg,jidx;
      jidx.clear();
      jpt.clear();
      jeta.clear();
      jMV1.clear();
      jpdg.clear();
      for(int sj=0;sj<(int) t_fatjets_RebuildAntiKt2Mitru_pt->size();sj++){
	if(t_fatjets_RebuildAntiKt2Mitru_pt->at(sj)>15*GeV&&t_fatjets_RebuildAntiKt2Mitru_pt->at(sj)<20*GeV){
	  jpt.push_back(20*GeV);
	  jeta.push_back(t_fatjets_RebuildAntiKt2Mitru_eta->at(sj));
	  jMV1.push_back(1);
	  jpdg.push_back(t_fatjets_RebuildAntiKt2Mitru_truth_label->at(sj));
	  jidx.push_back(sj);
	}
	else{
	  jpt.push_back(t_fatjets_RebuildAntiKt2Mitru_pt->at(sj));
	  jeta.push_back(t_fatjets_RebuildAntiKt2Mitru_eta->at(sj));
	  jMV1.push_back(1);
	  jpdg.push_back(t_fatjets_RebuildAntiKt2Mitru_truth_label->at(sj));
	  jidx.push_back(sj);
	}
      }
      
      
      
      int njets=t_fatjets_RebuildAntiKt4Mitru_pt->size();
      
      if(m_nj_max==-1){
	m_enj_max=njets;
      }
      else{
	m_enj_max=m_nj_max;
      }
      if(m_nb_max==-1){
	m_enb_max=njets;
      }
      else{
	m_enb_max=m_nb_max;
      }
      
      
      cbc.setJets(jpt, jeta, jpdg, jMV1);
      cbc.setSeed(jentry+jpt.size());
      vector<double> trfc_nom_ex, trfc_nom_in;
      trfc_nom_ex.clear();
      trfc_nom_in.clear();
      double bSF=cbc.getEvtSF("CumDef");
   
      vector<vector<bool> > perm_ex, perm_in;
      vector<vector<int> > tagbin_ex, tagbin_in;
    
      if(m_doTRF){
	cbc.getTRFweights("CumDef",jpt.size(),trfc_nom_ex,trfc_nom_in);

	cbc.chooseTagPermutation("CumDef",jpt.size(),perm_ex, perm_in);
	
      }
    
     
      
      vector<double> trfc_ex, trfc_in;
      trfc_ex.clear();
      trfc_in.clear();
   
      trfc_ex=trfc_nom_ex;
      trfc_in=trfc_nom_in;
     
      
      //cout<<"size nominal = "<<trfc_nom_ex.size()<<"   size copied = "<<trfc_ex.size()<<endl;
      ////END TRF STUFF////
      
      m_fatjets_n=-1;
      m_fatjets_pt.clear();
      m_fatjets_eta.clear();
      m_fatjets_phi.clear();
      m_fatjets_m.clear();


      
      ////MATCH FATJET
      vector<int> idx_fatjets_microjets;
      //if(m_mass>50){
      DoJetFatMatch(idx_fatjets_microjets,t_fatjets_RebuildCamKt8MitruSplitFilt_pt,t_fatjets_RebuildCamKt8MitruSplitFilt_eta,t_fatjets_RebuildCamKt8MitruSplitFilt_phi,t_fatjets_RebuildCamKt8MitruSplitFilt_m,0.6,t_fatjets_RebuildAntiKt2Mitru_pt,t_fatjets_RebuildAntiKt2Mitru_eta,t_fatjets_RebuildAntiKt2Mitru_phi,t_fatjets_RebuildAntiKt2Mitru_m);
      vector<int> idx_fatjets_jets_R8;
      DoJetFatMatch(idx_fatjets_jets_R8,t_fatjets_RebuildCamKt8MitruSplitFilt_pt,t_fatjets_RebuildCamKt8MitruSplitFilt_eta,t_fatjets_RebuildCamKt8MitruSplitFilt_phi,t_fatjets_RebuildCamKt8MitruSplitFilt_m,0.6,t_fatjets_RebuildAntiKt4Mitru_pt,t_fatjets_RebuildAntiKt4Mitru_eta,t_fatjets_RebuildAntiKt4Mitru_phi,t_fatjets_RebuildAntiKt4Mitru_m);

      m_fatjets_n=t_fatjets_RebuildCamKt8MitruSplitFilt_n;
      m_fatjets_pt=*t_fatjets_RebuildCamKt8MitruSplitFilt_pt;
      m_fatjets_eta=*t_fatjets_RebuildCamKt8MitruSplitFilt_eta;
      m_fatjets_phi=*t_fatjets_RebuildCamKt8MitruSplitFilt_phi;
      m_fatjets_m=*t_fatjets_RebuildCamKt8MitruSplitFilt_m;


	//}
      /*
      else{
      
      	DoJetFatMatch(idx_fatjets_microjets,t_fatjets_RebuildCamKt6MitruSplitFilt_pt,t_fatjets_RebuildCamKt6MitruSplitFilt_eta,t_fatjets_RebuildCamKt6MitruSplitFilt_phi,t_fatjets_RebuildCamKt6MitruSplitFilt_m,0.45);
	m_fatjets_n=t_fatjets_RebuildCamKt6MitruSplitFilt_n;
	m_fatjets_pt=*t_fatjets_RebuildCamKt6MitruSplitFilt_pt;
	m_fatjets_eta=*t_fatjets_RebuildCamKt6MitruSplitFilt_eta;
	m_fatjets_phi=*t_fatjets_RebuildCamKt6MitruSplitFilt_phi;
	m_fatjets_m=*t_fatjets_RebuildCamKt6MitruSplitFilt_m;


      }
      */
      vector<int> idx_fatjets_microjets_R6;
      DoJetFatMatch(idx_fatjets_microjets_R6,t_fatjets_RebuildCamKt6MitruSplitFilt_pt,t_fatjets_RebuildCamKt6MitruSplitFilt_eta,t_fatjets_RebuildCamKt6MitruSplitFilt_phi,t_fatjets_RebuildCamKt6MitruSplitFilt_m,0.45,t_fatjets_RebuildAntiKt2Mitru_pt,t_fatjets_RebuildAntiKt2Mitru_eta,t_fatjets_RebuildAntiKt2Mitru_phi,t_fatjets_RebuildAntiKt2Mitru_m);
      vector<int> idx_fatjets_jets_R6;
      DoJetFatMatch(idx_fatjets_jets_R6,t_fatjets_RebuildCamKt6MitruSplitFilt_pt,t_fatjets_RebuildCamKt6MitruSplitFilt_eta,t_fatjets_RebuildCamKt6MitruSplitFilt_phi,t_fatjets_RebuildCamKt6MitruSplitFilt_m,0.45,t_fatjets_RebuildAntiKt4Mitru_pt,t_fatjets_RebuildAntiKt4Mitru_eta,t_fatjets_RebuildAntiKt4Mitru_phi,t_fatjets_RebuildAntiKt4Mitru_m);



      //vector<int> idx_fatjets_microjets_R10;
      //DoJetFatMatch(idx_fatjets_microjets_R10,t_fatjets_RebuildCamKt10MitruSplitFilt_pt,t_fatjets_RebuildCamKt10MitruSplitFilt_eta,t_fatjets_RebuildCamKt10MitruSplitFilt_phi,t_fatjets_RebuildCamKt10MitruSplitFilt_m,0.75);
      vector<int> idx_jets_microjets;
      DoJetFatMatch(idx_jets_microjets,t_fatjets_RebuildAntiKt4Mitru_pt,t_fatjets_RebuildAntiKt4Mitru_eta,t_fatjets_RebuildAntiKt4Mitru_phi,t_fatjets_RebuildAntiKt4Mitru_m,0.3,t_fatjets_RebuildAntiKt2Mitru_pt,t_fatjets_RebuildAntiKt2Mitru_eta,t_fatjets_RebuildAntiKt2Mitru_phi,t_fatjets_RebuildAntiKt2Mitru_m);

      ////END MATCH FATJET
      
       //if(jentry==10){break;};
      float weight=t_mcevt_weight;//*t_RWPDFWeight;//* gli altri
      
      
      TLorentzVector leptop,hadtop,higgs,ttbar,ttH;
      leptop.SetPtEtaPhiM(t_p_leptop_pt,t_p_leptop_eta,t_p_leptop_phi,t_p_leptop_m);
      hadtop.SetPtEtaPhiM(t_p_hadtop_pt,t_p_hadtop_eta,t_p_hadtop_phi,t_p_hadtop_m);
      ttbar=leptop+hadtop;
      if(m_ds_num==2){
	higgs.SetPtEtaPhiM(t_p_higgs_pt,t_p_higgs_eta,t_p_higgs_phi,t_p_higgs_m);
	ttH=ttbar+higgs;
      }
      
      
      float ttweight=1.;
      ////////TOP AND TTBAR PT RW//////
      if(m_ds_num==1){
	weight*=ttweight;
      }
            
      ////////////////////////////////
      float mass_scale=1.;
      if(sys==1){
	mass_scale=1.05;
      }
      else if(sys==2){
	mass_scale=0.95;
      }
      

      ////////////////////////////////
      
      string m_sample_title;
      if(m_ds_num==1){
	//m_sample_title="ttlight_"+m_syst->at(sys);
	
	if(t_HF_class==0){
	  m_sample_title="ttlight_"+m_syst->at(sys);
	}
	else if(abs(t_HF_class)>0&&abs(t_HF_class<100)){
	  //cout<<" C category = "<<abs(t_HF_class)<<endl;
	  if(abs(t_HF_class)==1){
	    m_sample_title="ttC_"+m_syst->at(sys);
	  }
	  else if(abs(t_HF_class)==10){
	    m_sample_title="ttc_"+m_syst->at(sys);
	  }
	  else{
	    m_sample_title="ttcc_"+m_syst->at(sys);
	  }
	}
	else if(abs(t_HF_class)>=100){
	  if(abs(t_HF_class)==100){
	    m_sample_title="ttB_"+m_syst->at(sys);
	  }
	  else if(abs(t_HF_class)==1000){
	    m_sample_title="ttb_"+m_syst->at(sys);
	  }
	  else{
	    m_sample_title="ttbb_"+m_syst->at(sys);
	  }
	}
	
      }
      else{
	m_sample_title="ttH_"+m_syst->at(sys);
      }
      
      
      
      
      FillTH1D("cutflow_pheno"+m_sample_title,1,weight*trfc_in.at(0));
      
      
      //lep selection
      if(t_j_leptop_lep_pt>25&&fabs(t_j_leptop_lep_eta)<2.5){
	

	FillTH1D("cutflow_pheno"+m_sample_title,2,weight*trfc_in.at(0));
	
	//int n_bjets=-1;//GetNBtags();	 
	//float htall=GetHTall();

	
	for(int iTRF=0;iTRF<(int) perm_ex.size();iTRF++){
	  
	  m_h1d["hist_nbtag_TRF"+m_sample_title]->Fill(iTRF,weight*trfc_ex.at(iTRF));
	  
	  
	  for(int i=0;i<(int) t_fatjets_RebuildAntiKt2Mitru_pt->size();i++){
	    if(perm_ex.at(iTRF).at(i)==1){
	      FillTH1D("hist_Amicrojet_pt"+m_sample_title,t_fatjets_RebuildAntiKt2Mitru_pt->at(i)/GeV,weight);
	    }
	  }
	}
	
	
	
	
	double trf_weight;
	if(m_nb_min<=(int) t_fatjets_RebuildAntiKt2Mitru_pt->size()){
	  if(m_nb_min==m_nb_max){
	    trf_weight=trfc_ex.at(m_nb_min);
	  }
	  else{
	    trf_weight=trfc_in.at(m_nb_min);
	  }
	}

	

	if(t_fatjets_RebuildAntiKt2Mitru_pt->size()>=6){	
	  
	  FillTH1D("cutflow_pheno"+m_sample_title,3,weight*trfc_in.at(0));
	  FillTH1D("hist_nfatjet"+m_sample_title,m_fatjets_pt.size(),weight*trf_weight);
	  


	  /*
	  vector<float> truth_pt,truth_eta,truth_phi,truth_m;
	  truth_pt.clear();
	  truth_eta.clear();
	  truth_phi.clear();
	  truth_m.clear();
	  truth_pt.push_back(t_p_hadtop_q1_pt);truth_eta.push_back(t_p_hadtop_q1_eta);truth_phi.push_back(t_p_hadtop_q1_phi);truth_m.push_back(t_p_hadtop_q1_m);
	  truth_pt.push_back(t_p_hadtop_q2_pt);truth_eta.push_back(t_p_hadtop_q2_eta);truth_phi.push_back(t_p_hadtop_q2_phi);truth_m.push_back(t_p_hadtop_q2_m);
	  truth_pt.push_back(t_p_hadtop_b_pt);truth_eta.push_back(t_p_hadtop_b_eta);truth_phi.push_back(t_p_hadtop_b_phi);truth_m.push_back(t_p_hadtop_b_m);
	  truth_pt.push_back(t_p_leptop_b_pt);truth_eta.push_back(t_p_leptop_b_eta);truth_phi.push_back(t_p_leptop_b_phi);truth_m.push_back(t_p_leptop_b_m);
	  */
	  
	  //if(m_fatjets_pt.size()>0){
	  //FillTH1D("hist_leadjet_pt"+m_sample_title,m_fatjets_pt.at(0)/GeV,weight*trf_weight);
	  
	    
	    //cout<<"n jet min = "<<m_nj_min<<"  n jet max = "<<m_enj_max<<endl;
	    ///TRF BDRS SELECTION///
	    for(int iTRF=m_nb_min;iTRF<=m_enb_max && iTRF<(int) perm_ex.size();iTRF++){
	      FillTH1D("cutflow_pheno"+m_sample_title,4,weight*trfc_ex.at(iTRF));
	      //cout<<"iTRF = "<<iTRF<<endl;
	      int best_bdrs=-1;
	      float bdrs_pt=60.;//m_pt_mass;
	      int nbdrs=0;
	      for(int iBDRS=0;iBDRS<m_fatjets_n;iBDRS++){
		
		int bcontent_BDRS=GetFatJetBContentTRF(iBDRS,idx_fatjets_microjets,perm_ex.at(iTRF));
		FillTH1D("hist_nbtag_BDRS_TRF"+m_sample_title,bcontent_BDRS,weight*trfc_ex.at(iTRF));
		if(bcontent_BDRS>=2){
		  nbdrs++;
		  if(m_fatjets_pt.at(iBDRS)/GeV>bdrs_pt){
		    bdrs_pt=m_fatjets_pt.at(iBDRS)/GeV;
		    best_bdrs=iBDRS;
		  }///HIGHEST PT
		}//2tags inside
		
	      }///loop BDRS
	      FillTH1D("hist_nBDRS_TRF"+m_sample_title,nbdrs,weight*trfc_ex.at(iTRF));
	      
	      if(best_bdrs!=-1){
		
		
	
		FillTH1D("hist_leadBDRS_m_TRF"+m_sample_title,(m_fatjets_m.at(best_bdrs)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_pt_TRF"+m_sample_title,m_fatjets_pt.at(best_bdrs)/GeV,weight*trfc_ex.at(iTRF));
		
		
		FillTH1D("hist_njets"+m_sample_title,t_fatjets_RebuildAntiKt4Mitru_pt->size(),weight*trfc_ex.at(iTRF));
		FillTH1D("hist_nmicrojets"+m_sample_title,t_fatjets_RebuildAntiKt2Mitru_pt->size(),weight*trfc_ex.at(iTRF));
		
		int nevtag=0;
		for(int ijet=0;ijet<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();ijet++){
		  int bcontent_akt=GetFatJetBContentTRF(ijet,idx_jets_microjets,perm_ex.at(iTRF));
		  if(bcontent_akt>0){
		    nevtag++;
		  }
		}
		FillTH1D("hist_nbeventtag"+m_sample_title,nevtag,weight*trfc_ex.at(iTRF));
		
		//for(int iTRF=m_nb_min;iTRF<(int) perm_ex.size();iTRF++){
		  //cout<<"iTRF =  "<<iTRF<<endl;

		m_h1d["hist_nbmicrojets_TRF"+m_sample_title]->Fill(iTRF,weight*trfc_ex.at(iTRF));
		  
		  //}
		
			
		float aktDR=GetAntiKtDR(best_bdrs,idx_fatjets_microjets,perm_ex.at(iTRF));
		
		FillTH1D("hist_DRmujet_fatjet"+m_sample_title,aktDR,weight*trfc_ex.at(iTRF));
		int nj_BDRS=GetFatJetContent(best_bdrs,idx_fatjets_microjets);
		FillTH1D("hist_njets_BDRS_TRF"+m_sample_title,nj_BDRS,weight*trfc_ex.at(iTRF));
		
		
		
		
	      }//BEST BDRS


	      
	      //OPIMIZATION PART


	      /////R 8//////
	      int best_bdrs_R8_50=-1;
	      int best_bdrs_R8_60=-1;
	      int best_bdrs_R8_80=-1;
	      int best_bdrs_R8_100=-1;
	      int best_bdrs_R8_120=-1;
	      int best_bdrs_R8_150=-1;
	      int best_bdrs_R8_200=-1;
	      int best_bdrs_R8_250=-1;
	      float bdrs_pt_R8_50=50.;
	      float bdrs_pt_R8_60=60.;
	      float bdrs_pt_R8_80=80.;
	      float bdrs_pt_R8_100=100.;
	      float bdrs_pt_R8_120=120.;
	      float bdrs_pt_R8_150=150.;
	      float bdrs_pt_R8_200=200.;
	      float bdrs_pt_R8_250=250.;

	      //cout<<"Prima loop R8 "<<endl;
	      
	      for(int iBDRS=0;iBDRS<t_fatjets_RebuildCamKt8MitruSplitFilt_n;iBDRS++){
		int bcontent_BDRS=GetFatJetBContentTRF(iBDRS,idx_fatjets_microjets,perm_ex.at(iTRF));
	
		if(bcontent_BDRS>=2){
		  
		  if(t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R8_50){
		    bdrs_pt_R8_50=t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV;
		    best_bdrs_R8_50=iBDRS;
		    if(t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R8_60){
		      bdrs_pt_R8_60=t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV;
		      best_bdrs_R8_60=iBDRS;
		      if(t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R8_80){
			bdrs_pt_R8_80=t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV;
			best_bdrs_R8_80=iBDRS;
			if(t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R8_100){
			  bdrs_pt_R8_100=t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV;
			  best_bdrs_R8_100=iBDRS;
			  if(t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R8_120){
			    bdrs_pt_R8_120=t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV;
			    best_bdrs_R8_120=iBDRS;
			    if(t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R8_150){
			      bdrs_pt_R8_150=t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV;
			      best_bdrs_R8_150=iBDRS;
			      if(t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R8_200){
				bdrs_pt_R8_200=t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV;
				best_bdrs_R8_200=iBDRS;
				if(t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R8_250){
				  bdrs_pt_R8_250=t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(iBDRS)/GeV;
				  best_bdrs_R8_250=iBDRS;
				}//HIGHEST PT 250GeV

			      }///HIGHEST PT 200 GeV
			    }///HIGHEST PT 150 GeV
			  }///HIGHEST PT 120 GeV
			}///HIGHEST PT 100 GeV
		      }///HIGHEST PT 80 GeV
		    }///HIGHEST PT 60 GeV
		  }///HIGHEST PT 50 GeV
		}//2tags inside
		
	      }///loop BDRS
	      //cout<<"Dopo loop R8"<<endl;
	      if(best_bdrs_R8_50!=-1){
  	
		FillTH1D("hist_leadBDRS_R8_m_50GeV"+m_sample_title,(t_fatjets_RebuildCamKt8MitruSplitFilt_m->at(best_bdrs_R8_50)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_pt_50GeV"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(best_bdrs_R8_50)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R8 50 GeV
	      if(best_bdrs_R8_60!=-1){
  	
		FillTH1D("hist_leadBDRS_R8_m_60GeV"+m_sample_title,(t_fatjets_RebuildCamKt8MitruSplitFilt_m->at(best_bdrs_R8_60)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_pt_60GeV"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(best_bdrs_R8_60)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R8 60 GeV
	      if(best_bdrs_R8_80!=-1){
		
		FillTH1D("hist_leadBDRS_R8_m_80GeV"+m_sample_title,(t_fatjets_RebuildCamKt8MitruSplitFilt_m->at(best_bdrs_R8_80)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_pt_80GeV"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(best_bdrs_R8_80)/GeV,weight*trfc_ex.at(iTRF));
		
	      }//BEST BDRS R8 80 GeV
	      if(best_bdrs_R8_100!=-1){
		
		FillTH1D("hist_leadBDRS_R8_m_100GeV"+m_sample_title,(t_fatjets_RebuildCamKt8MitruSplitFilt_m->at(best_bdrs_R8_100)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_pt_100GeV"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(best_bdrs_R8_100)/GeV,weight*trfc_ex.at(iTRF));
		
	      }//BEST BDRS R8 100 GeV
	      if(best_bdrs_R8_120!=-1){
		
		FillTH1D("hist_leadBDRS_R8_m_120GeV"+m_sample_title,(t_fatjets_RebuildCamKt8MitruSplitFilt_m->at(best_bdrs_R8_120)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_pt_120GeV"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(best_bdrs_R8_120)/GeV,weight*trfc_ex.at(iTRF));
		
	      }//BEST BDRS R8 120 GeV
	      if(best_bdrs_R8_150!=-1){
		

		FillTH1D("cutflow_pheno"+m_sample_title,5,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_m_150GeV"+m_sample_title,(t_fatjets_RebuildCamKt8MitruSplitFilt_m->at(best_bdrs_R8_150)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_pt_150GeV"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(best_bdrs_R8_150)/GeV,weight*trfc_ex.at(iTRF));
	       

		FillTH1D("hist_leadBDRS_R8_150GeV_d12"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT12->at(best_bdrs_R8_150)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_150GeV_d23"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT23->at(best_bdrs_R8_150)/GeV,weight*trfc_ex.at(iTRF));
		if(t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1->at(best_bdrs_R8_150)>0){
		  FillTH1D("hist_leadBDRS_R8_150GeV_t21"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2->at(best_bdrs_R8_150)/t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1->at(best_bdrs_R8_150),weight*trfc_ex.at(iTRF));
		  if(t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2->at(best_bdrs_R8_150)>0){
		    FillTH1D("hist_leadBDRS_R8_150GeV_t32"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_Tau3->at(best_bdrs_R8_150)/t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2->at(best_bdrs_R8_150),weight*trfc_ex.at(iTRF));
		  }
		}

		vector<int> akt_out_R8_150;
		int aktcontent_BDRS=GetAntiKtMultiplicity(best_bdrs_R8_150,idx_fatjets_jets_R8,akt_out_R8_150);
		FillTH1D("hist_leadBDRS_R8_150GeV_nj"+m_sample_title,aktcontent_BDRS,weight*trfc_ex.at(iTRF));

		
		
		if(aktcontent_BDRS>3){
		  FillTH1D("cutflow_pheno"+m_sample_title,6,weight*trfc_ex.at(iTRF));
		  
		  TLorentzVector tv_jet1,lep,nu,h_top,l_top,h_W,lW;
		  vector<TLorentzVector> tv_jets;
		  tv_jets.clear();
		  vector<int> info;
		  info.clear();
		  lep.SetPtEtaPhiM(t_j_leptop_lep_pt,t_j_leptop_lep_eta,t_j_leptop_lep_phi,t_j_leptop_lep_m);
		  nu.SetPtEtaPhiM(t_p_leptop_nu_pt,t_p_leptop_nu_eta,t_p_leptop_nu_phi,t_p_leptop_nu_m);

		  for(int ij_idx=0;ij_idx<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();ij_idx++){
		    if(best_bdrs_R8_150!=idx_fatjets_jets_R8.at(ij_idx)){
		     
		      tv_jet1.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(ij_idx)/GeV,t_fatjets_RebuildAntiKt4Mitru_eta->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_phi->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_m->at(ij_idx)/GeV);
		      tv_jets.push_back(tv_jet1);
		      
		    }
		  }
		  double chi2;
		  chi2=DoChi2SemiLepMod(tv_jets,lep,nu,h_top,l_top,h_W,lW,info);
		  
		  
		  FillTH1D("hist_leadBDRS_R8_150GeV_chi2"+m_sample_title,chi2,weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_150GeV_mt"+m_sample_title,h_top.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_150GeV_mw"+m_sample_title,h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_150GeV_diff"+m_sample_title,h_top.M()-h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_150GeV_ml"+m_sample_title,l_top.M(),weight*trfc_ex.at(iTRF));
		  
		  
		}

	       


	      }//BEST BDRS R8 150 GeV
	      if(best_bdrs_R8_200!=-1){
		
		FillTH1D("cutflow_pheno"+m_sample_title,7,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_m_200GeV"+m_sample_title,(t_fatjets_RebuildCamKt8MitruSplitFilt_m->at(best_bdrs_R8_200)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_pt_200GeV"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(best_bdrs_R8_200)/GeV,weight*trfc_ex.at(iTRF));
		
		
		FillTH1D("hist_leadBDRS_R8_200GeV_d12"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT12->at(best_bdrs_R8_200)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_200GeV_d23"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT23->at(best_bdrs_R8_200)/GeV,weight*trfc_ex.at(iTRF));
		if(t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1->at(best_bdrs_R8_200)>0){
		  FillTH1D("hist_leadBDRS_R8_200GeV_t21"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2->at(best_bdrs_R8_200)/t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1->at(best_bdrs_R8_200),weight*trfc_ex.at(iTRF));
		  if(t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2->at(best_bdrs_R8_200)>0){
		    FillTH1D("hist_leadBDRS_R8_200GeV_t32"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_Tau3->at(best_bdrs_R8_200)/t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2->at(best_bdrs_R8_200),weight*trfc_ex.at(iTRF));
		  }
		}

		vector<int> akt_out_R8_200;
		int aktcontent_BDRS=GetAntiKtMultiplicity(best_bdrs_R8_200,idx_fatjets_jets_R8,akt_out_R8_200);
		FillTH1D("hist_leadBDRS_R8_200GeV_nj"+m_sample_title,aktcontent_BDRS,weight*trfc_ex.at(iTRF));

		
		
		if(aktcontent_BDRS>3){
		  FillTH1D("cutflow_pheno"+m_sample_title,8,weight*trfc_ex.at(iTRF));
		  
		  TLorentzVector tv_jet1,lep,nu,h_top,l_top,h_W,lW;
		  vector<TLorentzVector> tv_jets;
		  tv_jets.clear();
		  vector<int> info;
		  info.clear();
		  lep.SetPtEtaPhiM(t_j_leptop_lep_pt,t_j_leptop_lep_eta,t_j_leptop_lep_phi,t_j_leptop_lep_m);
		  nu.SetPtEtaPhiM(t_p_leptop_nu_pt,t_p_leptop_nu_eta,t_p_leptop_nu_phi,t_p_leptop_nu_m);

		  for(int ij_idx=0;ij_idx<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();ij_idx++){
		    if(best_bdrs_R8_200!=idx_fatjets_jets_R8.at(ij_idx)){
		     
		      tv_jet1.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(ij_idx)/GeV,t_fatjets_RebuildAntiKt4Mitru_eta->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_phi->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_m->at(ij_idx)/GeV);
		      tv_jets.push_back(tv_jet1);
		      
		    }
		  }
		  double chi2;
		  chi2=DoChi2SemiLepMod(tv_jets,lep,nu,h_top,l_top,h_W,lW,info);
		  
		  
		  FillTH1D("hist_leadBDRS_R8_200GeV_chi2"+m_sample_title,chi2,weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_200GeV_mt"+m_sample_title,h_top.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_200GeV_mw"+m_sample_title,h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_200GeV_diff"+m_sample_title,h_top.M()-h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_200GeV_ml"+m_sample_title,l_top.M(),weight*trfc_ex.at(iTRF));
		  
		  
		}





	      }//BEST BDRS R8 200 GeV
	      if(best_bdrs_R8_250!=-1){
  	
		FillTH1D("cutflow_pheno"+m_sample_title,9,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_m_250GeV"+m_sample_title,(t_fatjets_RebuildCamKt8MitruSplitFilt_m->at(best_bdrs_R8_250)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_pt_250GeV"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_pt->at(best_bdrs_R8_250)/GeV,weight*trfc_ex.at(iTRF));
		
		
		FillTH1D("hist_leadBDRS_R8_250GeV_d12"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT12->at(best_bdrs_R8_250)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R8_250GeV_d23"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT23->at(best_bdrs_R8_250)/GeV,weight*trfc_ex.at(iTRF));
		if(t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1->at(best_bdrs_R8_250)>0){
		  FillTH1D("hist_leadBDRS_R8_250GeV_t21"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2->at(best_bdrs_R8_250)/t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1->at(best_bdrs_R8_250),weight*trfc_ex.at(iTRF));
		  if(t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2->at(best_bdrs_R8_250)>0){
		    FillTH1D("hist_leadBDRS_R8_250GeV_t32"+m_sample_title,t_fatjets_RebuildCamKt8MitruSplitFilt_Tau3->at(best_bdrs_R8_250)/t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2->at(best_bdrs_R8_250),weight*trfc_ex.at(iTRF));
		  }
		}

		vector<int> akt_out_R8_250;
		int aktcontent_BDRS=GetAntiKtMultiplicity(best_bdrs_R8_250,idx_fatjets_jets_R8,akt_out_R8_250);
		FillTH1D("hist_leadBDRS_R8_250GeV_nj"+m_sample_title,aktcontent_BDRS,weight*trfc_ex.at(iTRF));

		
		
		if(aktcontent_BDRS>3){
		  FillTH1D("cutflow_pheno"+m_sample_title,10,weight*trfc_ex.at(iTRF));
		  
		  TLorentzVector tv_jet1,lep,nu,h_top,l_top,h_W,lW;
		  vector<TLorentzVector> tv_jets;
		  tv_jets.clear();
		  vector<int> info;
		  info.clear();
		  lep.SetPtEtaPhiM(t_j_leptop_lep_pt,t_j_leptop_lep_eta,t_j_leptop_lep_phi,t_j_leptop_lep_m);
		  nu.SetPtEtaPhiM(t_p_leptop_nu_pt,t_p_leptop_nu_eta,t_p_leptop_nu_phi,t_p_leptop_nu_m);

		  for(int ij_idx=0;ij_idx<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();ij_idx++){
		    if(best_bdrs_R8_250!=idx_fatjets_jets_R8.at(ij_idx)){
		     
		      tv_jet1.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(ij_idx)/GeV,t_fatjets_RebuildAntiKt4Mitru_eta->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_phi->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_m->at(ij_idx)/GeV);
		      tv_jets.push_back(tv_jet1);
		      
		    }
		  }
		  double chi2;
		  chi2=DoChi2SemiLepMod(tv_jets,lep,nu,h_top,l_top,h_W,lW,info);
		  
		  
		  FillTH1D("hist_leadBDRS_R8_250GeV_chi2"+m_sample_title,chi2,weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_250GeV_mt"+m_sample_title,h_top.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_250GeV_mw"+m_sample_title,h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_250GeV_diff"+m_sample_title,h_top.M()-h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R8_250GeV_ml"+m_sample_title,l_top.M(),weight*trfc_ex.at(iTRF));
		  
		  
		}

	       
	      }//BEST BDRS R8 250 GeV

	      //////END R 8//////



	      /////R 6//////
	     
	      int best_bdrs_R6_60=-1;
	      int best_bdrs_R6_70=-1;
	      int best_bdrs_R6_80=-1;
	      int best_bdrs_R6_100=-1;
	      int best_bdrs_R6_120=-1;
	      int best_bdrs_R6_150=-1;
	      int best_bdrs_R6_200=-1;
	      float bdrs_pt_R6_60=60.;
	      float bdrs_pt_R6_70=70.;
	      float bdrs_pt_R6_80=80.;
	      float bdrs_pt_R6_100=100.;
	      float bdrs_pt_R6_120=120.;
	      float bdrs_pt_R6_150=150.;
	      float bdrs_pt_R6_200=200.;
	      //cout<<"Prima loop R6 "<<endl;
	      for(int iBDRS=0;iBDRS<t_fatjets_RebuildCamKt6MitruSplitFilt_n;iBDRS++){
		int bcontent_BDRS=GetFatJetBContentTRF(iBDRS,idx_fatjets_microjets_R6,perm_ex.at(iTRF));
		if(bcontent_BDRS>=2){
		  
		  
		  if(t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R6_60){
		    bdrs_pt_R6_60=t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV;
		    best_bdrs_R6_60=iBDRS;
		    if(t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R6_70){
		      bdrs_pt_R6_70=t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV;
		      best_bdrs_R6_70=iBDRS;
		      if(t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R6_80){
			bdrs_pt_R6_80=t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV;
			best_bdrs_R6_80=iBDRS;
			
			if(t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R6_100){
			  bdrs_pt_R6_100=t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV;
			  best_bdrs_R6_100=iBDRS;
			  if(t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R6_120){
			    bdrs_pt_R6_120=t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV;
			    best_bdrs_R6_120=iBDRS;
			    if(t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R6_150){
			      bdrs_pt_R6_150=t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV;
			      best_bdrs_R6_150=iBDRS;
			      if(t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R6_200){
				bdrs_pt_R6_200=t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(iBDRS)/GeV;
				best_bdrs_R6_200=iBDRS;
			      }///HIGHEST PT 200 GeV
			      
			    }///HIGHEST PT 150 GeV
			  }///HIGHEST PT 120 GeV
			}///HIGHEST PT 100 GeV
		      }///HIGHEST PT 80 GeV
		    }///HIGHEST PT 70 GeV
		  }///HIGHEST PT 60 GeV
		  
		  
		}//2tags inside
		
	      }///loop BDRS
	      //cout<<"Dopo loop R6 "<<endl;
	      if(best_bdrs_R6_60!=-1){
		FillTH1D("cutflow_pheno"+m_sample_title,11,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_m_60GeV"+m_sample_title,(t_fatjets_RebuildCamKt6MitruSplitFilt_m->at(best_bdrs_R6_60)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_pt_60GeV"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(best_bdrs_R6_60)/GeV,weight*trfc_ex.at(iTRF));



		FillTH1D("hist_leadBDRS_R6_60GeV_d12"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12->at(best_bdrs_R6_60)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_60GeV_d23"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23->at(best_bdrs_R6_60)/GeV,weight*trfc_ex.at(iTRF));
		if(t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1->at(best_bdrs_R6_60)>0){
		  FillTH1D("hist_leadBDRS_R6_60GeV_t21"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_60)/t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1->at(best_bdrs_R6_60),weight*trfc_ex.at(iTRF));
		  if(t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_60)>0){
		  FillTH1D("hist_leadBDRS_R6_60GeV_t32"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3->at(best_bdrs_R6_60)/t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_60),weight*trfc_ex.at(iTRF));
		  }
		}
		
		vector<int> akt_out_R6_60;
		int aktcontent_BDRS=GetAntiKtMultiplicity(best_bdrs_R6_60,idx_fatjets_jets_R6,akt_out_R6_60);
		FillTH1D("hist_leadBDRS_R6_60GeV_nj"+m_sample_title,aktcontent_BDRS,weight*trfc_ex.at(iTRF));



		if(aktcontent_BDRS>3){
		  FillTH1D("cutflow_pheno"+m_sample_title,12,weight*trfc_ex.at(iTRF));
   
		  
		  TLorentzVector tv_jet1,lep,nu,h_top,l_top,h_W,lW;
		  vector<TLorentzVector> tv_jets;
		  tv_jets.clear();
		  vector<int> info;
		  info.clear();
		  lep.SetPtEtaPhiM(t_j_leptop_lep_pt,t_j_leptop_lep_eta,t_j_leptop_lep_phi,t_j_leptop_lep_m);
		  nu.SetPtEtaPhiM(t_p_leptop_nu_pt,t_p_leptop_nu_eta,t_p_leptop_nu_phi,t_p_leptop_nu_m);
		  
		  for(int ij_idx=0;ij_idx<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();ij_idx++){
		    if(best_bdrs_R6_60!=idx_fatjets_jets_R6.at(ij_idx)){
		      
		      tv_jet1.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(ij_idx)/GeV,t_fatjets_RebuildAntiKt4Mitru_eta->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_phi->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_m->at(ij_idx)/GeV);
		      tv_jets.push_back(tv_jet1);
		      
		    }
		  }
		   
		  double chi2;
		  chi2=DoChi2SemiLepMod(tv_jets,lep,nu,h_top,l_top,h_W,lW,info);
		  
		  
		  
		  FillTH1D("hist_leadBDRS_R6_60GeV_chi2"+m_sample_title,chi2,weight*trfc_ex.at(iTRF));
		  
		  
		  FillTH1D("hist_leadBDRS_R6_60GeV_mt"+m_sample_title,h_top.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_60GeV_mw"+m_sample_title,h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_60GeV_diff"+m_sample_title,h_top.M()-h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_60GeV_ml"+m_sample_title,l_top.M(),weight*trfc_ex.at(iTRF));
		  
		  
		  
		  
		  
		}
		
		
       	      }//BEST BDRS R6 60 GeV
	      if(best_bdrs_R6_70!=-1){
		
		FillTH1D("hist_leadBDRS_R6_m_70GeV"+m_sample_title,(t_fatjets_RebuildCamKt6MitruSplitFilt_m->at(best_bdrs_R6_70)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_pt_70GeV"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(best_bdrs_R6_70)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R6 70 GeV
	      if(best_bdrs_R6_80!=-1){
		
		FillTH1D("hist_leadBDRS_R6_m_80GeV"+m_sample_title,(t_fatjets_RebuildCamKt6MitruSplitFilt_m->at(best_bdrs_R6_80)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_pt_80GeV"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(best_bdrs_R6_80)/GeV,weight*trfc_ex.at(iTRF));
		
	      }//BEST BDRS R6 80 GeV
	      if(best_bdrs_R6_100!=-1){
		FillTH1D("cutflow_pheno"+m_sample_title,13,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_m_100GeV"+m_sample_title,(t_fatjets_RebuildCamKt6MitruSplitFilt_m->at(best_bdrs_R6_100)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_pt_100GeV"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(best_bdrs_R6_100)/GeV,weight*trfc_ex.at(iTRF));
		
		
		FillTH1D("hist_leadBDRS_R6_100GeV_d12"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12->at(best_bdrs_R6_100)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_100GeV_d23"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23->at(best_bdrs_R6_100)/GeV,weight*trfc_ex.at(iTRF));
		if(t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1->at(best_bdrs_R6_100)>0){
		  FillTH1D("hist_leadBDRS_R6_100GeV_t21"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_100)/t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1->at(best_bdrs_R6_100),weight*trfc_ex.at(iTRF));
		  if(t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_100)>0){
		    FillTH1D("hist_leadBDRS_R6_100GeV_t32"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3->at(best_bdrs_R6_100)/t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_100),weight*trfc_ex.at(iTRF));
		  }
		}
		
		vector<int> akt_out_R6_100;
		int aktcontent_BDRS=GetAntiKtMultiplicity(best_bdrs_R6_100,idx_fatjets_jets_R6,akt_out_R6_100);
		FillTH1D("hist_leadBDRS_R6_100GeV_nj"+m_sample_title,aktcontent_BDRS,weight*trfc_ex.at(iTRF));
		
		
		
		
		
		if(aktcontent_BDRS>3){
		  FillTH1D("cutflow_pheno"+m_sample_title,14,weight*trfc_ex.at(iTRF));
		  TLorentzVector tv_jet1,lep,nu,h_top,l_top,h_W,lW;
		  vector<TLorentzVector> tv_jets;
	 	  tv_jets.clear();
		  vector<int> info;
		  info.clear();
		  lep.SetPtEtaPhiM(t_j_leptop_lep_pt,t_j_leptop_lep_eta,t_j_leptop_lep_phi,t_j_leptop_lep_m);
		  nu.SetPtEtaPhiM(t_p_leptop_nu_pt,t_p_leptop_nu_eta,t_p_leptop_nu_phi,t_p_leptop_nu_m);
		  
		  for(int ij_idx=0;ij_idx<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();ij_idx++){
		    if(best_bdrs_R6_100!=idx_fatjets_jets_R6.at(ij_idx)){
		     
		      tv_jet1.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(ij_idx)/GeV,t_fatjets_RebuildAntiKt4Mitru_eta->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_phi->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_m->at(ij_idx)/GeV);
		      tv_jets.push_back(tv_jet1);
		      
		    }
		  }
		   
		  double chi2;
		  chi2=DoChi2SemiLepMod(tv_jets,lep,nu,h_top,l_top,h_W,lW,info);
		  
		  
		  
		  FillTH1D("hist_leadBDRS_R6_100GeV_chi2"+m_sample_title,chi2,weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_100GeV_mt"+m_sample_title,h_top.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_100GeV_mw"+m_sample_title,h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_100GeV_diff"+m_sample_title,h_top.M()-h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_100GeV_ml"+m_sample_title,l_top.M(),weight*trfc_ex.at(iTRF));
		  
		  
		  
		}
		
		
	      }//BEST BDRS R6 100 GeV
	      if(best_bdrs_R6_120!=-1){
		FillTH1D("cutflow_pheno"+m_sample_title,15,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_m_120GeV"+m_sample_title,(t_fatjets_RebuildCamKt6MitruSplitFilt_m->at(best_bdrs_R6_120)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_pt_120GeV"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(best_bdrs_R6_120)/GeV,weight*trfc_ex.at(iTRF));
		
		FillTH1D("hist_leadBDRS_R6_120GeV_d12"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12->at(best_bdrs_R6_120)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_120GeV_d23"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23->at(best_bdrs_R6_120)/GeV,weight*trfc_ex.at(iTRF));
		if(t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1->at(best_bdrs_R6_120)>0){
		  FillTH1D("hist_leadBDRS_R6_120GeV_t21"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_120)/t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1->at(best_bdrs_R6_120),weight*trfc_ex.at(iTRF));
		  if(t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_120)>0){
		    FillTH1D("hist_leadBDRS_R6_120GeV_t32"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3->at(best_bdrs_R6_120)/t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_120),weight*trfc_ex.at(iTRF));
		  }
		}
		
		vector<int> akt_out_R6_120;
		int aktcontent_BDRS=GetAntiKtMultiplicity(best_bdrs_R6_120,idx_fatjets_jets_R6,akt_out_R6_120);
		FillTH1D("hist_leadBDRS_R6_120GeV_nj"+m_sample_title,aktcontent_BDRS,weight*trfc_ex.at(iTRF));
		
		
		
		if(aktcontent_BDRS>3){
		  
		  FillTH1D("cutflow_pheno"+m_sample_title,16,weight*trfc_ex.at(iTRF));
		  TLorentzVector tv_jet1,lep,nu,h_top,l_top,h_W,lW;
		  vector<TLorentzVector> tv_jets;
	 	  tv_jets.clear();
		  vector<int> info;
		  info.clear();
		  lep.SetPtEtaPhiM(t_j_leptop_lep_pt,t_j_leptop_lep_eta,t_j_leptop_lep_phi,t_j_leptop_lep_m);
		  nu.SetPtEtaPhiM(t_p_leptop_nu_pt,t_p_leptop_nu_eta,t_p_leptop_nu_phi,t_p_leptop_nu_m);
		  
		  for(int ij_idx=0;ij_idx<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();ij_idx++){
		    if(best_bdrs_R6_120!=idx_fatjets_jets_R6.at(ij_idx)){
		      
		      tv_jet1.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(ij_idx)/GeV,t_fatjets_RebuildAntiKt4Mitru_eta->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_phi->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_m->at(ij_idx)/GeV);
		      tv_jets.push_back(tv_jet1);
		      
		    }
		  }
		  
		  
		  double chi2;
		  chi2=DoChi2SemiLepMod(tv_jets,lep,nu,h_top,l_top,h_W,lW,info);
		  
		  
		  
		  FillTH1D("hist_leadBDRS_R6_120GeV_chi2"+m_sample_title,chi2,weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_120GeV_mt"+m_sample_title,h_top.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_120GeV_mw"+m_sample_title,h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_120GeV_diff"+m_sample_title,h_top.M()-h_W.M(),weight*trfc_ex.at(iTRF));
		  FillTH1D("hist_leadBDRS_R6_120GeV_ml"+m_sample_title,l_top.M(),weight*trfc_ex.at(iTRF));
		  
		  
		}


	      }//BEST BDRS R6 120 GeV
	      if(best_bdrs_R6_150!=-1){
  	
		FillTH1D("hist_leadBDRS_R6_m_150GeV"+m_sample_title,(t_fatjets_RebuildCamKt6MitruSplitFilt_m->at(best_bdrs_R6_150)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_pt_150GeV"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(best_bdrs_R6_150)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R6 150 GeV
	      if(best_bdrs_R6_200!=-1){
		FillTH1D("cutflow_pheno"+m_sample_title,17,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_m_200GeV"+m_sample_title,(t_fatjets_RebuildCamKt6MitruSplitFilt_m->at(best_bdrs_R6_200)/GeV)*mass_scale,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_pt_200GeV"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_pt->at(best_bdrs_R6_200)/GeV,weight*trfc_ex.at(iTRF));


		FillTH1D("hist_leadBDRS_R6_200GeV_d12"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12->at(best_bdrs_R6_200)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R6_200GeV_d23"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23->at(best_bdrs_R6_200)/GeV,weight*trfc_ex.at(iTRF));
		if(t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1->at(best_bdrs_R6_200)>0){
		  FillTH1D("hist_leadBDRS_R6_200GeV_t21"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_200)/t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1->at(best_bdrs_R6_200),weight*trfc_ex.at(iTRF));
		  if(t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_200)>0){
		    FillTH1D("hist_leadBDRS_R6_200GeV_t32"+m_sample_title,t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3->at(best_bdrs_R6_200)/t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2->at(best_bdrs_R6_200),weight*trfc_ex.at(iTRF));
		  }
		}
		
		vector<int> akt_out_R6_200;
		int aktcontent_BDRS=GetAntiKtMultiplicity(best_bdrs_R6_200,idx_fatjets_jets_R6,akt_out_R6_200);
		FillTH1D("hist_leadBDRS_R6_200GeV_nj"+m_sample_title,aktcontent_BDRS,weight*trfc_ex.at(iTRF));




		
		if(aktcontent_BDRS>3){
		  FillTH1D("cutflow_pheno"+m_sample_title,18,weight*trfc_ex.at(iTRF));
		  TLorentzVector tv_jet1,lep,nu,h_top,l_top,h_W,lW;
		  vector<TLorentzVector> tv_jets;
	 	  tv_jets.clear();
		  vector<int> info;
		  info.clear();
		  lep.SetPtEtaPhiM(t_j_leptop_lep_pt,t_j_leptop_lep_eta,t_j_leptop_lep_phi,t_j_leptop_lep_m);
		  nu.SetPtEtaPhiM(t_p_leptop_nu_pt,t_p_leptop_nu_eta,t_p_leptop_nu_phi,t_p_leptop_nu_m);
		  
		  for(int ij_idx=0;ij_idx<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();ij_idx++){
		    if(best_bdrs_R6_200!=idx_fatjets_jets_R6.at(ij_idx)){
		      
		      tv_jet1.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(ij_idx)/GeV,t_fatjets_RebuildAntiKt4Mitru_eta->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_phi->at(ij_idx),t_fatjets_RebuildAntiKt4Mitru_m->at(ij_idx)/GeV);
		      tv_jets.push_back(tv_jet1);
		      
		    }
		  }

		  
		    double chi2;
		    chi2=DoChi2SemiLepMod(tv_jets,lep,nu,h_top,l_top,h_W,lW,info);
		    
		   
		    FillTH1D("hist_leadBDRS_R6_200GeV_chi2"+m_sample_title,chi2,weight*trfc_ex.at(iTRF));
		    FillTH1D("hist_leadBDRS_R6_200GeV_mt"+m_sample_title,h_top.M(),weight*trfc_ex.at(iTRF));
		    FillTH1D("hist_leadBDRS_R6_200GeV_mw"+m_sample_title,h_W.M(),weight*trfc_ex.at(iTRF));
		    FillTH1D("hist_leadBDRS_R6_200GeV_diff"+m_sample_title,h_top.M()-h_W.M(),weight*trfc_ex.at(iTRF));
		    FillTH1D("hist_leadBDRS_R6_200GeV_ml"+m_sample_title,l_top.M(),weight*trfc_ex.at(iTRF));
	
		  
		  
		}
		
		
		
	      }//BEST BDRS R6 200 GeV
	      

	      //////END R 6//////

	      /*

	      /////R 10//////
	      int best_bdrs_R10_40=-1;
	      int best_bdrs_R10_50=-1;
	      int best_bdrs_R10_60=-1;
	      int best_bdrs_R10_80=-1;
	      int best_bdrs_R10_100=-1;
	      int best_bdrs_R10_120=-1;
	      int best_bdrs_R10_150=-1;
	      int best_bdrs_R10_200=-1;
	      float bdrs_pt_R10_40=40.;
	      float bdrs_pt_R10_50=50.;
	      float bdrs_pt_R10_60=60.;
	      float bdrs_pt_R10_80=80.;
	      float bdrs_pt_R10_100=100.;
	      float bdrs_pt_R10_120=120.;
	      float bdrs_pt_R10_150=150.;
	      float bdrs_pt_R10_200=200.;
	      //cout<<"Prima loop R10 "<<endl;
	      for(int iBDRS=0;iBDRS<t_fatjets_RebuildCamKt10MitruSplitFilt_n;iBDRS++){
		int bcontent_BDRS=GetFatJetBContentTRF(iBDRS,idx_fatjets_microjets_R10,perm_ex.at(iTRF));
	
		if(bcontent_BDRS>=2){
		  
		  if(t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R10_40){
		    bdrs_pt_R10_40=t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV;
		    best_bdrs_R10_40=iBDRS;
		    if(t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R10_50){
		      bdrs_pt_R10_50=t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV;
		      best_bdrs_R10_50=iBDRS;
		      if(t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R10_60){
			bdrs_pt_R10_60=t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV;
			best_bdrs_R10_60=iBDRS;
			if(t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R10_80){
			  bdrs_pt_R10_80=t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV;
			  best_bdrs_R10_80=iBDRS;
			  
			  if(t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R10_100){
			    bdrs_pt_R10_100=t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV;
			    best_bdrs_R10_100=iBDRS;
			    if(t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R10_120){
			      bdrs_pt_R10_120=t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV;
			      best_bdrs_R10_120=iBDRS;
			      if(t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R10_150){
				bdrs_pt_R10_150=t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV;
				best_bdrs_R10_150=iBDRS;
				if(t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV>bdrs_pt_R10_200){
				  bdrs_pt_R10_200=t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(iBDRS)/GeV;
				  best_bdrs_R10_200=iBDRS;
				 }///HIGHEST PT 200 GeV
			      }///HIGHEST PT 150 GeV
			    }///HIGHEST PT 120 GeV
			  }///HIGHEST PT 100 GeV
			}///HIGHEST PT 80 GeV
		      }///HIGHEST PT 60 GeV
		    }///HIGHEST PT 50 GeV
		  }///HIGHEST PT 40 GeV
		}//2tags inside
		
	      }///loop BDRS
	      //cout<<"Prima loop R10 "<<endl;
	      if(best_bdrs_R10_40!=-1){
  	
		FillTH1D("hist_leadBDRS_R10_m_40GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_m->at(best_bdrs_R10_40)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R10_pt_40GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(best_bdrs_R10_40)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R10 40 GeV
	      if(best_bdrs_R10_50!=-1){
  	
		FillTH1D("hist_leadBDRS_R10_m_50GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_m->at(best_bdrs_R10_50)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R10_pt_50GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(best_bdrs_R10_50)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R10 50 GeV
	      if(best_bdrs_R10_60!=-1){
  	
		FillTH1D("hist_leadBDRS_R10_m_60GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_m->at(best_bdrs_R10_60)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R10_pt_60GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(best_bdrs_R10_60)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R10 60 GeV
	      if(best_bdrs_R10_80!=-1){
		
		FillTH1D("hist_leadBDRS_R10_m_80GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_m->at(best_bdrs_R10_80)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R10_pt_80GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(best_bdrs_R10_80)/GeV,weight*trfc_ex.at(iTRF));
		
	      }//BEST BDRS R10 80 GeV
	      if(best_bdrs_R10_100!=-1){
		
		FillTH1D("hist_leadBDRS_R10_m_100GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_m->at(best_bdrs_R10_100)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R10_pt_100GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(best_bdrs_R10_100)/GeV,weight*trfc_ex.at(iTRF));
		
	      }//BEST BDRS R10 100 GeV
	      if(best_bdrs_R10_120!=-1){
		
		FillTH1D("hist_leadBDRS_R10_m_120GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_m->at(best_bdrs_R10_120)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R10_pt_120GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(best_bdrs_R10_120)/GeV,weight*trfc_ex.at(iTRF));
		
	      }//BEST BDRS R10 120 GeV
	      if(best_bdrs_R10_150!=-1){
  	
		FillTH1D("hist_leadBDRS_R10_m_150GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_m->at(best_bdrs_R10_150)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R10_pt_150GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(best_bdrs_R10_150)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R10 150 GeV
	      if(best_bdrs_R10_200!=-1){
  	
		FillTH1D("hist_leadBDRS_R10_m_200GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_m->at(best_bdrs_R10_200)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R10_pt_200GeV"+m_sample_title,t_fatjets_RebuildCamKt10MitruSplitFilt_pt->at(best_bdrs_R10_200)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R10 200 GeV


	      //////END R 10//////

	      

	      /////R 4//////
	     
	    
	      int best_bdrs_R4_100=-1;
	      int best_bdrs_R4_120=-1;
	      int best_bdrs_R4_150=-1;
	      int best_bdrs_R4_200=-1;
	    
	      float bdrs_pt_R4_100=100.;
	      float bdrs_pt_R4_120=120.;
	      float bdrs_pt_R4_150=150.;
	      float bdrs_pt_R4_200=200.;
	    
	      for(int iBDRS=0;iBDRS<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();iBDRS++){
		int bcontent_BDRS=GetFatJetBContentTRF(iBDRS,idx_jets_microjets,perm_ex.at(iTRF));
		
		if(bcontent_BDRS>=2){
		  
		  if(t_fatjets_RebuildAntiKt4Mitru_pt->at(iBDRS)/GeV>bdrs_pt_R4_100){
		    bdrs_pt_R4_100=t_fatjets_RebuildAntiKt4Mitru_pt->at(iBDRS)/GeV;
		    best_bdrs_R4_100=iBDRS;
		    if(t_fatjets_RebuildAntiKt4Mitru_pt->at(iBDRS)/GeV>bdrs_pt_R4_120){
		      bdrs_pt_R4_120=t_fatjets_RebuildAntiKt4Mitru_pt->at(iBDRS)/GeV;
		      best_bdrs_R4_120=iBDRS;
		      if(t_fatjets_RebuildAntiKt4Mitru_pt->at(iBDRS)/GeV>bdrs_pt_R4_150){
			bdrs_pt_R4_150=t_fatjets_RebuildAntiKt4Mitru_pt->at(iBDRS)/GeV;
			best_bdrs_R4_150=iBDRS;
			if(t_fatjets_RebuildAntiKt4Mitru_pt->at(iBDRS)/GeV>bdrs_pt_R4_200){
			  bdrs_pt_R4_200=t_fatjets_RebuildAntiKt4Mitru_pt->at(iBDRS)/GeV;
			  best_bdrs_R4_200=iBDRS;
			}///HIGHEST PT 200 GeV
			
		      }///HIGHEST PT 150 GeV
		    }///HIGHEST PT 120 GeV
		  }///HIGHEST PT 100 GeV
		  
		  
		  
		}//2tags inside
		
	      }///loop BDRS
	      
	      if(best_bdrs_R4_100!=-1){
		
		FillTH1D("hist_leadBDRS_R4_m_100GeV"+m_sample_title,t_fatjets_RebuildAntiKt4Mitru_m->at(best_bdrs_R4_100)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R4_pt_100GeV"+m_sample_title,t_fatjets_RebuildAntiKt4Mitru_pt->at(best_bdrs_R4_100)/GeV,weight*trfc_ex.at(iTRF));
		
	      }//BEST BDRS R4 100 GeV
	      if(best_bdrs_R4_120!=-1){
		
		FillTH1D("hist_leadBDRS_R4_m_120GeV"+m_sample_title,t_fatjets_RebuildAntiKt4Mitru_m->at(best_bdrs_R4_120)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R4_pt_120GeV"+m_sample_title,t_fatjets_RebuildAntiKt4Mitru_pt->at(best_bdrs_R4_120)/GeV,weight*trfc_ex.at(iTRF));
		
	      }//BEST BDRS R4 120 GeV
	      if(best_bdrs_R4_150!=-1){
  	
		FillTH1D("hist_leadBDRS_R4_m_150GeV"+m_sample_title,t_fatjets_RebuildAntiKt4Mitru_m->at(best_bdrs_R4_150)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R4_pt_150GeV"+m_sample_title,t_fatjets_RebuildAntiKt4Mitru_pt->at(best_bdrs_R4_150)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R4 150 GeV
	      if(best_bdrs_R4_200!=-1){
  	
		FillTH1D("hist_leadBDRS_R4_m_200GeV"+m_sample_title,t_fatjets_RebuildAntiKt4Mitru_m->at(best_bdrs_R4_200)/GeV,weight*trfc_ex.at(iTRF));
		FillTH1D("hist_leadBDRS_R4_pt_200GeV"+m_sample_title,t_fatjets_RebuildAntiKt4Mitru_pt->at(best_bdrs_R4_200)/GeV,weight*trfc_ex.at(iTRF));
	       
	      }//BEST BDRS R4 200 GeV


	      //////END R 4//////

	      */
	      //END OPTIMIZATION PART
	      

	    }//TRF PERMUTATION
	    
	    
	    
	    //cout<<"n bjet min = "<<m_nb_min<<"  n bjet max = "<<m_enb_max<<endl;
	    
	    
	    //}///1 FATJET
	  
	}//AT LEAST Nx JETS
	

	
    
	
      }//lep selection
      if(jentry%100000==0){
	cout<<"PROCESSED "<< jentry<<" ENTRIES"<<endl;
      }
    }//loop entries
   
  }//loop systematics
}

void Reader::Terminate()
{
  cout<<"IN TERMINATE:"<<endl;
  
  vector<string> samples;
  samples.clear();

  if(m_ds_num==1){
      samples.push_back("ttlight");
      samples.push_back("ttb");
      samples.push_back("ttbb");
      samples.push_back("ttB");
      samples.push_back("ttc");
      samples.push_back("ttcc");
      samples.push_back("ttC");
    }
    else{
      samples.push_back("ttH");
    }




  map<string,TH1D *>::iterator it;
  map<string,TFile *>::iterator ite;
  for(int sys=0;sys<(int) m_syst->size();sys++){
    for(int j=0; j< (int) samples.size();j++){
      //cout<<"Writing file : "<<m_out_folder+"/"+samples.at(j)+m_suffix_file+"_"+m_syst->at(sys)<<endl;
      ite=m_output.find(m_out_folder+"/"+samples.at(j)+m_suffix_file+"_"+m_syst->at(sys));
      //for(map<string,TFile *>::iterator ite=m_output.begin(); ite!=m_output.end(); ++ite){
      (ite->second)->cd();
      
      for(int i=0; i<(int) histo_names->size();i++){
	//cout<<"Writing histo : "<<histo_names->at(i)+samples.at(j)+"_"+m_syst->at(sys)<<endl;
	it=m_h1d.find(histo_names->at(i)+samples.at(j)+"_"+m_syst->at(sys));
	(it->second)->Write(histo_names->at(i).c_str()); delete it->second; m_h1d.erase(it);
      }

      
      /*
	hist_2d_HT_higgs->Write();
	hist_2d_HT_top->Write();
	hist_2d_ung_fatjet_higgs->Write();
	hist_2d_ung_fatjet_top->Write();
	hist_2d_ung_fatjetsum_higgs->Write();
	hist_2d_ung_fatjetsum_top->Write();
	hist_2d_bdrs_fatjet_higgs->Write();
	hist_2d_bdrs_fatjet_top->Write();
	hist_2d_bdrs_fatjetsum_higgs->Write();
	hist_2d_bdrs_fatjetsum_top->Write();
      */
      (ite->second)->Close();  m_output.erase(ite); //delete it->second;
    }
  }
  
  
  
  

  if(m_ds_name=="ttbar"){
    delete m_truthreweighter;
  }
  
  delete m_syst;
}


void Reader::DoJetFatMatch(vector<int> &match_idx,vector<float> *pt,vector<float> *eta,vector<float> *phi,vector<float> *m,float DR,vector<float> *pt_ref,vector<float> *eta_ref,vector<float> *phi_ref,vector<float> *m_ref){
  match_idx.clear();
  for(int i=0;i<(int) pt_ref->size();i++){
    TLorentzVector jet;
    jet.SetPtEtaPhiM(pt_ref->at(i)/GeV,eta_ref->at(i),phi_ref->at(i),m_ref->at(i)/GeV);
    float tmp_DR=DR;
    int closest=-9;
    for(int j=0;j<(int) pt->size();j++){
      TLorentzVector fatjet;
      fatjet.SetPtEtaPhiM(pt->at(j)/GeV,eta->at(j),phi->at(j),m->at(j)/GeV);
      
      if(jet.DeltaR(fatjet)<tmp_DR){
	tmp_DR=jet.DeltaR(fatjet);
	closest=j;
      }
    }
    match_idx.push_back(closest);
    
  }
  
}


float Reader::GetFatDR(int fatjet_index,vector<int> match_idx){
  TLorentzVector fat;
  TLorentzVector jet;
  float DR=999;
  /*
  for(int j=0;j<(int) t_fatjets_RebuildCamKt15LCTopo_pt->size();j++){
    if(fatjet_index==t_fatjets_RebuildCamKt15LCTopo_orig_ind->at(j)&&fatjet_index!=-1){
      fat.SetPtEtaPhiM(t_fatjets_RebuildCamKt15LCTopo_pt->at(j)/GeV,t_fatjets_RebuildCamKt15LCTopo_eta->at(j),t_fatjets_RebuildCamKt15LCTopo_phi->at(j),t_fatjets_RebuildCamKt15LCTopo_m->at(j)/GeV);
      for(int i=0;i<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();i++){
	if(fatjet_index==match_idx.at(i)){
	  
	  jet.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(i)/GeV,t_fatjets_RebuildAntiKt4Mitru_eta->at(i),t_fatjets_RebuildAntiKt4Mitru_phi->at(i),t_fatjets_RebuildAntiKt4Mitru_m->at(i)/GeV);
	  float tmp_DR=jet.DeltaR(fat);
	  if(tmp_DR<DR){
	    DR=tmp_DR;
	  }
	}
      }
    }
  */
  return DR;
}

float Reader::GetAntiKtDR(int fatjet_index,vector<int> match_idx,vector<bool> permutation){
 
  float DR=999;
  vector<int> selected;
  selected.clear();
  if(permutation.size()>0){
    for(int i=0;i<(int) t_fatjets_RebuildAntiKt2Mitru_pt->size();i++){
      if(fatjet_index==match_idx.at(i)){
	if(permutation.at(i)==1){
	  selected.push_back(i);
	}
      }
    } 
  }


  if(selected.size()>1){
    TLorentzVector firstjet;
    TLorentzVector secondjet;
    firstjet.SetPtEtaPhiM(t_fatjets_RebuildAntiKt2Mitru_pt->at(selected.at(0)),t_fatjets_RebuildAntiKt2Mitru_eta->at(selected.at(0)),t_fatjets_RebuildAntiKt2Mitru_phi->at(selected.at(0)),t_fatjets_RebuildAntiKt2Mitru_m->at(selected.at(0)));
    secondjet.SetPtEtaPhiM(t_fatjets_RebuildAntiKt2Mitru_pt->at(selected.at(1)),t_fatjets_RebuildAntiKt2Mitru_eta->at(selected.at(1)),t_fatjets_RebuildAntiKt2Mitru_phi->at(selected.at(1)),t_fatjets_RebuildAntiKt2Mitru_m->at(selected.at(1)));
    DR=firstjet.DeltaR(secondjet);
    
  }


  
  return DR;
}

float Reader::GetAntiKtMass(int fatjet_index,vector<int> match_idx){
  TLorentzVector leadb;
  TLorentzVector subb;
  int idx_lead=-1;
  int idx_sub=-1;
  for(int i=0;i<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();i++){
    if(fatjet_index==match_idx.at(i)){
      //if(t_fatjets_RebuildAntiKt4Mitru_MV1->at(i)>0.7892){
	if(idx_lead<0){
	  leadb.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(i),t_fatjets_RebuildAntiKt4Mitru_eta->at(i),t_fatjets_RebuildAntiKt4Mitru_phi->at(i),t_fatjets_RebuildAntiKt4Mitru_m->at(i));
	  idx_lead++;
	}
	if(idx_lead>-1&&idx_sub<0){
	  subb.SetPtEtaPhiM(t_fatjets_RebuildAntiKt4Mitru_pt->at(i),t_fatjets_RebuildAntiKt4Mitru_eta->at(i),t_fatjets_RebuildAntiKt4Mitru_phi->at(i),t_fatjets_RebuildAntiKt4Mitru_m->at(i));
	  idx_sub++;
	}
	//}
    }
  }
  if(leadb.Pt()>0&&subb.Pt()>0){
    TLorentzVector higgs=leadb+subb;
    return higgs.M();
  }

  else{
    return 0;
  }
}




int Reader::GetFatJetBContentTRF(int fatjet_index,vector<int>match_idx,vector<bool> permutation){
  int btags=0;
  if(permutation.size()!=0){
    for(int i=0;i<(int) t_fatjets_RebuildAntiKt2Mitru_pt->size();i++){
      if(fatjet_index==match_idx.at(i)){
	if(permutation.at(i)==1){	  
	  btags++;	  
	}
      }
    }
  }
  return btags;
}

int Reader::GetFatJetContent(int fatjet_index,vector<int>match_idx){
  int jets=0;
  
  for(int i=0;i<(int) t_fatjets_RebuildAntiKt2Mitru_pt->size();i++){
    if(fatjet_index==match_idx.at(i)){
      jets++;	  
    }
  }
  
  return jets;
}



int Reader::GetAntiKtMultiplicity(int fatjet_index,vector<int>match_idx,vector<int> &selected_idx){
  int jets=0;
  selected_idx.clear();
  for(int i=0;i<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();i++){
    if(fatjet_index!=match_idx.at(i)){
      jets++;	  
      selected_idx.push_back(i);
    }
    else{
      selected_idx.push_back(-1);
    }
  }
  
  return jets;
}


void Reader::DoJetTruthMatch(vector<int> &match_idx,vector<float> *pt,vector<float> *eta,vector<float> *phi,vector<float> *m,float DR,vector<float> pt_ref,vector<float> eta_ref,vector<float> phi_ref,vector<float> m_ref,vector<int> excl_idx){
  match_idx.clear();
  for(int i=0;i<(int) pt_ref.size();i++){
    TLorentzVector truth;
    truth.SetPtEtaPhiM(pt_ref.at(i),eta_ref.at(i),phi_ref.at(i),m_ref.at(i));
    float tmp_DR=DR;
    int closest=-9;
    for(int j=0;j<(int) pt->size();j++){
      if(excl_idx.at(j)==j){
	TLorentzVector jet;
	jet.SetPtEtaPhiM(pt->at(j)/GeV,eta->at(j),phi->at(j),m->at(j)/GeV);
	
	if(truth.DeltaR(jet)<tmp_DR){
	  tmp_DR=truth.DeltaR(jet);
	  closest=j;
	}
      }
    }
    match_idx.push_back(closest);
    
  }
  
}








float Reader::GetHTall(){

  float htall=0;
  for(int i=0;i<(int) t_fatjets_RebuildAntiKt4Mitru_pt->size();i++){
    htall+=(t_fatjets_RebuildAntiKt4Mitru_pt->at(i)/GeV);
  }
  return htall;
}




void Reader::BookTH1D(string name, int nbins, double xlow, double xup, const char* title, const char* xtitle, const char* ytitle, int lw, int lc){
  TH1D* h1=new TH1D(name.c_str(), title, nbins,xlow,xup);
  h1->GetXaxis()->SetTitle(xtitle); h1->GetYaxis()->SetTitle(ytitle);
  h1->SetLineWidth(lw); h1->SetLineColor(lc);
  h1->Sumw2();
  string s=name; m_h1d[s]=h1; s.clear();
  return;
}

void Reader::BookTH1D(string name, int nbins, double* xedges, const char* title ,const char* xtitle, const char* ytitle, int lw, int lc){
  TH1D* h1=new TH1D(name.c_str(), title, nbins,xedges);
  h1->GetXaxis()->SetTitle(xtitle); h1->GetYaxis()->SetTitle(ytitle);
  h1->SetLineWidth(lw); h1->SetLineColor(lc);
  h1->Sumw2();
  string s=name; m_h1d[s]=h1; s.clear();
  return;
}


void Reader::FillTH1D(string hname, double val, double weight){
  if(m_h1d[hname]!=0){m_h1d[hname]->Fill(val,weight);}
  else{m_h1d.erase(m_h1d.find(hname));  cout<<"Error: TH1D "<<hname<<" not found to fill"<<endl;}
  return;
}



void Reader::BookOutput(string name){
  string fname=name+".root";
  TFile *f1=new TFile(fname.c_str(),"RECREATE");
  string s=name; m_output[s]=f1; s.clear();
  return;
}

void Reader::FillHistoNames(){
  histo_names=new vector<string>;
  histo_names->clear();
  

  histo_names->push_back("cutflow_pheno");
 
  

 
  histo_names->push_back("hist_nbtag_TRF");
  histo_names->push_back("hist_nbtag_BDRS_TRF");
  histo_names->push_back("hist_njets_BDRS_TRF");
  histo_names->push_back("hist_leadBDRS_m_TRF");
  histo_names->push_back("hist_leadBDRS_pt_TRF");
  histo_names->push_back("hist_nBDRS_TRF");

  
  
  histo_names->push_back("hist_nbmicrojets_TRF");
  
  histo_names->push_back("hist_higgs_m");
  histo_names->push_back("hist_nbtag");
  histo_names->push_back("hist_nfatjet");
  histo_names->push_back("hist_leadjet_pt");
  histo_names->push_back("hist_njets");
  histo_names->push_back("hist_nmicrojets");
  histo_names->push_back("hist_nbeventtag");
  histo_names->push_back("hist_Amicrojet_pt");
  histo_names->push_back("hist_DRmujet_fatjet");



  


  histo_names->push_back("hist_leadBDRS_R6_m_60GeV");
  histo_names->push_back("hist_leadBDRS_R6_pt_60GeV");
  histo_names->push_back("hist_leadBDRS_R6_m_70GeV");
  histo_names->push_back("hist_leadBDRS_R6_pt_70GeV");
  histo_names->push_back("hist_leadBDRS_R6_m_80GeV");
  histo_names->push_back("hist_leadBDRS_R6_pt_80GeV");
  histo_names->push_back("hist_leadBDRS_R6_m_100GeV");
  histo_names->push_back("hist_leadBDRS_R6_pt_100GeV");
  histo_names->push_back("hist_leadBDRS_R6_m_120GeV");
  histo_names->push_back("hist_leadBDRS_R6_pt_120GeV");
  histo_names->push_back("hist_leadBDRS_R6_m_150GeV");
  histo_names->push_back("hist_leadBDRS_R6_pt_150GeV");
  histo_names->push_back("hist_leadBDRS_R6_m_200GeV");
  histo_names->push_back("hist_leadBDRS_R6_pt_200GeV");
  histo_names->push_back("hist_leadBDRS_R8_m_50GeV");
  histo_names->push_back("hist_leadBDRS_R8_pt_50GeV");
  histo_names->push_back("hist_leadBDRS_R8_m_60GeV");
  histo_names->push_back("hist_leadBDRS_R8_pt_60GeV");
  histo_names->push_back("hist_leadBDRS_R8_m_80GeV");
  histo_names->push_back("hist_leadBDRS_R8_pt_80GeV");
  histo_names->push_back("hist_leadBDRS_R8_m_100GeV");
  histo_names->push_back("hist_leadBDRS_R8_pt_100GeV");
  histo_names->push_back("hist_leadBDRS_R8_m_120GeV");
  histo_names->push_back("hist_leadBDRS_R8_pt_120GeV");
  histo_names->push_back("hist_leadBDRS_R8_m_150GeV");
  histo_names->push_back("hist_leadBDRS_R8_pt_150GeV");
  histo_names->push_back("hist_leadBDRS_R8_m_200GeV");
  histo_names->push_back("hist_leadBDRS_R8_pt_200GeV");
  histo_names->push_back("hist_leadBDRS_R8_m_250GeV");
  histo_names->push_back("hist_leadBDRS_R8_pt_250GeV");




  histo_names->push_back("hist_leadBDRS_R6_60GeV_d12");
  histo_names->push_back("hist_leadBDRS_R6_60GeV_d23");
  histo_names->push_back("hist_leadBDRS_R6_60GeV_t21");
  histo_names->push_back("hist_leadBDRS_R6_60GeV_t32");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_d12");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_d23");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_t21");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_t32");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_d12");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_d23");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_t21");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_t32");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_d12");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_d23");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_t21");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_t32");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_d12");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_d23");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_t21");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_t32");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_d12");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_d23");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_t21");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_t32");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_d12");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_d23");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_t21");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_t32");



  histo_names->push_back("hist_leadBDRS_R6_60GeV_nj");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_nj");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_nj");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_nj");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_nj");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_nj");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_nj");




  histo_names->push_back("hist_leadBDRS_R6_60GeV_chi2");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_chi2");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_chi2");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_chi2");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_chi2");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_chi2");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_chi2");
  
  histo_names->push_back("hist_leadBDRS_R6_60GeV_mw");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_mw");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_mw");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_mw");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_mw");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_mw");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_mw");
  
  histo_names->push_back("hist_leadBDRS_R6_60GeV_mt");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_mt");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_mt");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_mt");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_mt");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_mt");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_mt");
  
  histo_names->push_back("hist_leadBDRS_R6_60GeV_diff");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_diff");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_diff");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_diff");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_diff");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_diff");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_diff");
  
  histo_names->push_back("hist_leadBDRS_R6_60GeV_ml");
  histo_names->push_back("hist_leadBDRS_R6_100GeV_ml");
  histo_names->push_back("hist_leadBDRS_R6_120GeV_ml");
  histo_names->push_back("hist_leadBDRS_R6_200GeV_ml");
  histo_names->push_back("hist_leadBDRS_R8_150GeV_ml");
  histo_names->push_back("hist_leadBDRS_R8_200GeV_ml");
  histo_names->push_back("hist_leadBDRS_R8_250GeV_ml");
  
 
  /*
    histo_names->push_back("hist_leadBDRS_R4_m_100GeV");
  histo_names->push_back("hist_leadBDRS_R4_pt_100GeV");
  histo_names->push_back("hist_leadBDRS_R4_m_120GeV");
  histo_names->push_back("hist_leadBDRS_R4_pt_120GeV");
  histo_names->push_back("hist_leadBDRS_R4_m_150GeV");
  histo_names->push_back("hist_leadBDRS_R4_pt_150GeV");
  histo_names->push_back("hist_leadBDRS_R4_m_200GeV");
  histo_names->push_back("hist_leadBDRS_R4_pt_200GeV");
  histo_names->push_back("hist_leadBDRS_R10_m_40GeV");
  histo_names->push_back("hist_leadBDRS_R10_pt_40GeV");
  histo_names->push_back("hist_leadBDRS_R10_m_50GeV");
  histo_names->push_back("hist_leadBDRS_R10_pt_50GeV");
  histo_names->push_back("hist_leadBDRS_R10_m_60GeV");
  histo_names->push_back("hist_leadBDRS_R10_pt_60GeV");
  histo_names->push_back("hist_leadBDRS_R10_m_80GeV");
  histo_names->push_back("hist_leadBDRS_R10_pt_80GeV");
  histo_names->push_back("hist_leadBDRS_R10_m_100GeV");
  histo_names->push_back("hist_leadBDRS_R10_pt_100GeV");
  histo_names->push_back("hist_leadBDRS_R10_m_120GeV");
  histo_names->push_back("hist_leadBDRS_R10_pt_120GeV");
  histo_names->push_back("hist_leadBDRS_R10_m_150GeV");
  histo_names->push_back("hist_leadBDRS_R10_pt_150GeV");
  histo_names->push_back("hist_leadBDRS_R10_m_200GeV");
  histo_names->push_back("hist_leadBDRS_R10_pt_200GeV");


  */

  return;
}






void Reader::FillSystNames(){
  m_syst=new vector<string>;
  m_syst->clear();
   
  m_syst->push_back("NOMINAL");
  if(m_doSys){
    m_syst->push_back("JMSUP");
    m_syst->push_back("JMSDOWN");  
  }
  /*
  if(m_doTRF&&m_doSys){
    m_syst->push_back("BTAGBREAK1UP");
    m_syst->push_back("BTAGBREAK2UP");
    m_syst->push_back("BTAGBREAK3UP");
    m_syst->push_back("BTAGBREAK4UP");
    m_syst->push_back("BTAGBREAK5UP");
    m_syst->push_back("BTAGBREAK6UP");

    m_syst->push_back("BTAGBREAK1DOWN");    
    m_syst->push_back("BTAGBREAK2DOWN");
    m_syst->push_back("BTAGBREAK3DOWN");
    m_syst->push_back("BTAGBREAK4DOWN");
    m_syst->push_back("BTAGBREAK5DOWN");
    m_syst->push_back("BTAGBREAK6DOWN");
    
    
    m_syst->push_back("CTAGBREAK1UP");
    m_syst->push_back("CTAGBREAK2UP");
    m_syst->push_back("CTAGBREAK3UP");
    m_syst->push_back("CTAGBREAK4UP");

    m_syst->push_back("CTAGBREAK1DOWN");
    m_syst->push_back("CTAGBREAK2DOWN");
    m_syst->push_back("CTAGBREAK3DOWN");
    m_syst->push_back("CTAGBREAK4DOWN");
    
    m_syst->push_back("LTAGBREAK1UP");
    m_syst->push_back("LTAGBREAK2UP");
    m_syst->push_back("LTAGBREAK3UP");
    m_syst->push_back("LTAGBREAK4UP");
    m_syst->push_back("LTAGBREAK5UP");
    m_syst->push_back("LTAGBREAK6UP");
    m_syst->push_back("LTAGBREAK7UP");
    m_syst->push_back("LTAGBREAK8UP");
    m_syst->push_back("LTAGBREAK9UP");
    m_syst->push_back("LTAGBREAK10UP");
    m_syst->push_back("LTAGBREAK11UP");
    m_syst->push_back("LTAGBREAK12UP");

    m_syst->push_back("LTAGBREAK1DOWN");
    m_syst->push_back("LTAGBREAK2DOWN");
    m_syst->push_back("LTAGBREAK3DOWN");
    m_syst->push_back("LTAGBREAK4DOWN");
    m_syst->push_back("LTAGBREAK5DOWN");
    m_syst->push_back("LTAGBREAK6DOWN");
    m_syst->push_back("LTAGBREAK7DOWN");
    m_syst->push_back("LTAGBREAK8DOWN");
    m_syst->push_back("LTAGBREAK9DOWN");
    m_syst->push_back("LTAGBREAK10DOWN");
    m_syst->push_back("LTAGBREAK11DOWN");
    m_syst->push_back("LTAGBREAK12DOWN");
    
    if(m_ds_name=="ttbar"){
      m_syst->push_back("ISRFRUP");
      m_syst->push_back("ISRFRDOWN");
      m_syst->push_back("FragmentationUP");
      m_syst->push_back("FragmentationDOWN");
      m_syst->push_back("MCGeneratorUP");
      m_syst->push_back("MCGeneratorDOWN");
      m_syst->push_back("JERUP");
      m_syst->push_back("JERDOWN");
      m_syst->push_back("bJESUP");
      m_syst->push_back("bJESDOWN");
      m_syst->push_back("closebyJESUP");
      m_syst->push_back("closebyJESDOWN");
      m_syst->push_back("etacalibJESUP");
      m_syst->push_back("etacalibJESDOWN");
      m_syst->push_back("effdetset1JESUP");
      m_syst->push_back("effdetset1JESDOWN");
      m_syst->push_back("BTAGEFFUP");
      m_syst->push_back("BTAGEFFDOWN");
      m_syst->push_back("POWHEGHERWIG");
    }
    
  }
*/
  return;
}
