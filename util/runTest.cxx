#include "TCanvas.h"
#include "TChain.h"
#include "TEnv.h"
#include "TFileCollection.h"
#include "TFrame.h"
#include "TString.h"
#include "THashList.h"
#include "TList.h"
#include "TPad.h"
#include "TPaveText.h"
#include "TROOT.h"
#include "TNamed.h"
#include "TParameter.h"
#include "TSystem.h"
#include "TApplication.h"
#include <iostream>
#include <fstream>
#include <stdarg.h>
#include "Reader/Reader.h"


int main(int argc,char **argv){

  gROOT->SetBatch(1);

  
  TString tree="";
  bool trf=false;
  bool syst=false;
  string ds="";
  string suffix="";
  string input_file="";
  string output_folder="";
  int initial=0;
  int final=-1;
  int nj_min=0;
  int nj_max=-1;
  int nb_min=0;
  int nb_max=-1;
  int mass=0;
  tree="TruthAnaTree";
  for(Int_t i = 1; i < argc; i++) {
    if(std::string(argv[i])=="-ds") {
      if(i+1 < argc) ds = argv[i+1];
    }
    if(std::string(argv[i])=="-i") {
      if(i+1 < argc) input_file = argv[i+1];
    }
    if(std::string(argv[i])=="-o") {
      if(i+1 < argc) output_folder = argv[i+1];
    }
    if(std::string(argv[i])=="-t") {
      if(i+1 < argc) tree = argv[i+1];
    }
    if(std::string(argv[i])=="-suf") {
      if(i+1 < argc) suffix = argv[i+1];
    }
    if(std::string(argv[i])=="-ni") {
      if(i+1 < argc) initial = atoi(std::string(argv[i+1]).c_str());
    }
    if(std::string(argv[i])=="-nf") {
      if(i+1 < argc) final = atoi(std::string(argv[i+1]).c_str());
    }
    if(std::string(argv[i])=="-ij") {
      if(i+1 < argc) nj_min = atoi(std::string(argv[i+1]).c_str());
    }
    if(std::string(argv[i])=="-fj") {
      if(i+1 < argc) nj_max = atoi(std::string(argv[i+1]).c_str());
    }
    if(std::string(argv[i])=="-ib") {
      if(i+1 < argc) nb_min = atoi(std::string(argv[i+1]).c_str());
    }
    if(std::string(argv[i])=="-fb") {
      if(i+1 < argc) nb_max = atoi(std::string(argv[i+1]).c_str());
    }
    //if(std::string(argv[i])=="-mass") {
    //  if(i+1 < argc) mass = atoi(std::string(argv[i+1]).c_str());
    //}
    if(std::string(argv[i])=="-doTRF") {
      trf = true;
    }  
    if(std::string(argv[i])=="-doSys") {
      syst = true;
     } 
  }
  
  
  if(ds=="" || tree=="" || input_file=="" || suffix=="" ||output_folder=="") {
    std::cout<<"ERROR: wrong options"<<std::endl;
    std::cout<<"right usage:\n"<<
      "./runTest -ds [dataset] -i [input_file] -o[output_folder] -t [tree] -suf [output_suffix]"<<std::endl;
     return 1;
  }
  
  
  //std::ifstream input;
  
  //input.open(input_file);
  TChain *ch = new TChain(tree,"");
  ch->Add(input_file.c_str());
  //std::string ntuple;	
  //input >> ntuple;
  //while(!input.eof()){  
  //cout<<ntuple<<endl;
  //ch->Add(ntuple.c_str());
    //input >> ntuple;
    //}
  
  cout<<"Entries in tutta la chain = "<<ch->GetEntries()<<endl;
  
  string s_dir="mkdir ";
  string s_toexec=s_dir+output_folder;
  gSystem->Exec(s_toexec.c_str());
  
  Reader *theTester= new Reader(ch);
  theTester->SetSSEvent(initial,final);
  theTester->SetDS(ds);
  theTester->SetOutFold(output_folder);
  theTester->SetNJets(nj_min,nj_max,nb_min,nb_max);
  theTester->SetTRF(trf);
  theTester->SetSys(syst);
  theTester->SetSuffix(suffix);
  theTester->SetMass(mass);
  theTester->Begin();
  std::cout<<"In Loop:"<<std::endl;
  theTester->Loop();
  theTester->Terminate();
  


    
  //gApplication->Terminate();
  return 0;
}
