#include <TLorentzVector.h>
#include <vector>

std::vector<double> lepnu(TLorentzVector& l, TLorentzVector& nu,std::vector<int> &info);
double calcChi2(TLorentzVector& wh, TLorentzVector& th, TLorentzVector& tlep);
double DoChi2SemiLep(TLorentzVector& j1, TLorentzVector& j2,TLorentzVector& j3,TLorentzVector& j4,TLorentzVector& l,TLorentzVector& nu,TLorentzVector& hadronic_top,TLorentzVector& leptonic_top,TLorentzVector& hadronic_w,TLorentzVector& leptonic_w,std::vector<int> &info);
double DoChi2SemiLepMod(std::vector<TLorentzVector> &jets,TLorentzVector& l,TLorentzVector& nu,TLorentzVector& hadronic_top,TLorentzVector& leptonic_top,TLorentzVector& hadronic_w,TLorentzVector& leptonic_w,std::vector<int> &info);

