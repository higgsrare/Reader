#ifndef Reader_h
#define Reader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <fstream>
#include <map>


// Header file for the classes stored in the TTree if any.
#include <vector>

#include "Reader/TopTruthReweighting.h"

using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.

class Reader {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
  
   // Declaration of leaf types
   Double_t        t_p_leptop_pt;
   Double_t        t_p_leptop_eta;
   Double_t        t_p_leptop_phi;
   Double_t        t_p_leptop_m;
   Double_t        t_p_leptop_hard_pt;
   Double_t        t_p_leptop_hard_eta;
   Double_t        t_p_leptop_hard_phi;
   Double_t        t_p_leptop_hard_m;
   Double_t        t_p_leptop_b_pt;
   Double_t        t_p_leptop_b_eta;
   Double_t        t_p_leptop_b_phi;
   Double_t        t_p_leptop_b_m;
   Double_t        t_p_leptop_W_pt;
   Double_t        t_p_leptop_W_eta;
   Double_t        t_p_leptop_W_phi;
   Double_t        t_p_leptop_W_m;
   Double_t        t_p_leptop_lep_pt;
   Double_t        t_p_leptop_lep_eta;
   Double_t        t_p_leptop_lep_phi;
   Double_t        t_p_leptop_lep_m;
   Int_t           t_p_leptop_lep_flav;
   Double_t        t_p_leptop_nu_pt;
   Double_t        t_p_leptop_nu_eta;
   Double_t        t_p_leptop_nu_phi;
   Double_t        t_p_leptop_nu_m;
   Double_t        t_p_hadtop_pt;
   Double_t        t_p_hadtop_eta;
   Double_t        t_p_hadtop_phi;
   Double_t        t_p_hadtop_m;
   Double_t        t_p_hadtop_hard_pt;
   Double_t        t_p_hadtop_hard_eta;
   Double_t        t_p_hadtop_hard_phi;
   Double_t        t_p_hadtop_hard_m;
   Double_t        t_p_hadtop_b_pt;
   Double_t        t_p_hadtop_b_eta;
   Double_t        t_p_hadtop_b_phi;
   Double_t        t_p_hadtop_b_m;
   Double_t        t_p_hadtop_W_pt;
   Double_t        t_p_hadtop_W_eta;
   Double_t        t_p_hadtop_W_phi;
   Double_t        t_p_hadtop_W_m;
   Double_t        t_p_hadtop_q1_pt;
   Double_t        t_p_hadtop_q1_eta;
   Double_t        t_p_hadtop_q1_phi;
   Double_t        t_p_hadtop_q1_m;
   Int_t           t_p_hadtop_q1_flav;
   Double_t        t_p_hadtop_q2_pt;
   Double_t        t_p_hadtop_q2_eta;
   Double_t        t_p_hadtop_q2_phi;
   Double_t        t_p_hadtop_q2_m;
   Int_t           t_p_hadtop_q2_flav;
   Double_t        t_p_higgs_pt;
   Double_t        t_p_higgs_eta;
   Double_t        t_p_higgs_phi;
   Double_t        t_p_higgs_m;
   Double_t        t_p_higgs_hard_pt;
   Double_t        t_p_higgs_hard_eta;
   Double_t        t_p_higgs_hard_phi;
   Double_t        t_p_higgs_hard_m;
   Double_t        t_p_higgs_b1_pt;
   Double_t        t_p_higgs_b1_eta;
   Double_t        t_p_higgs_b1_phi;
   Double_t        t_p_higgs_b1_m;
   Double_t        t_p_higgs_b2_pt;
   Double_t        t_p_higgs_b2_eta;
   Double_t        t_p_higgs_b2_phi;
   Double_t        t_p_higgs_b2_m;
   Int_t           t_j_leptop_b_ind;
   Double_t        t_pj_leptop_b_dr;
   Double_t        t_j_leptop_lep_pt;
   Double_t        t_j_leptop_lep_eta;
   Double_t        t_j_leptop_lep_phi;
   Double_t        t_j_leptop_lep_m;
   Int_t           t_j_leptop_lep_flav;
   Double_t        t_j_leptop_lep_charge;
   Double_t        t_j_leptop_nu_pt;
   Double_t        t_j_leptop_nu_phi;
   Double_t        t_pj_leptop_lep_dr;
   Double_t        t_pj_leptop_nu_dphi;
   Int_t           t_j_hadtop_b_ind;
   Double_t        t_pj_hadtop_b_dr;
   Int_t           t_j_hadtop_q1_ind;
   Double_t        t_pj_hadtop_q1_dr;
   Int_t           t_j_hadtop_q2_ind;
   Double_t        t_pj_hadtop_q2_dr;
   Int_t           t_j_higgs_b1_ind;
   Double_t        t_pj_higgs_b1_dr;
   Int_t           t_j_higgs_b2_ind;
   Double_t        t_pj_higgs_b2_dr;
   vector<float>   *t_jets_pt;
   vector<float>   *t_jets_eta;
   vector<float>   *t_jets_phi;
   vector<float>   *t_jets_m;
   vector<float>   *t_jets_MV1;
   vector<float>   *t_jets_trueflavor;
   Int_t           t_fatjets_RebuildCamKt8MitruSplitFilt_n;
   vector<float>   *t_fatjets_RebuildCamKt8MitruSplitFilt_pt;
   vector<float>   *t_fatjets_RebuildCamKt8MitruSplitFilt_eta;
   vector<float>   *t_fatjets_RebuildCamKt8MitruSplitFilt_phi;
   vector<float>   *t_fatjets_RebuildCamKt8MitruSplitFilt_m;
   vector<float>   *t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT12;
   vector<float>   *t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT23;
   vector<float>   *t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1;
   vector<float>   *t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2;
   vector<float>   *t_fatjets_RebuildCamKt8MitruSplitFilt_Tau3;
   vector<int>     *t_fatjets_RebuildCamKt8MitruSplitFilt_MCclass;
   vector<int>     *t_fatjets_RebuildCamKt8MitruSplitFilt_orig_ind;
   vector<int>     *t_fatjets_RebuildCamKt8MitruSplitFilt_assoc_ind;
   vector<int>     *t_fatjets_RebuildCamKt8MitruSplitFilt_truth_label;
   Int_t           t_fatjets_RebuildCamKt8MitruTrim_n;
   vector<float>   *t_fatjets_RebuildCamKt8MitruTrim_pt;
   vector<float>   *t_fatjets_RebuildCamKt8MitruTrim_eta;
   vector<float>   *t_fatjets_RebuildCamKt8MitruTrim_phi;
   vector<float>   *t_fatjets_RebuildCamKt8MitruTrim_m;
   vector<float>   *t_fatjets_RebuildCamKt8MitruTrim_SPLIT12;
   vector<float>   *t_fatjets_RebuildCamKt8MitruTrim_SPLIT23;
   vector<float>   *t_fatjets_RebuildCamKt8MitruTrim_Tau1;
   vector<float>   *t_fatjets_RebuildCamKt8MitruTrim_Tau2;
   vector<float>   *t_fatjets_RebuildCamKt8MitruTrim_Tau3;
   vector<int>     *t_fatjets_RebuildCamKt8MitruTrim_MCclass;
   vector<int>     *t_fatjets_RebuildCamKt8MitruTrim_orig_ind;
   vector<int>     *t_fatjets_RebuildCamKt8MitruTrim_assoc_ind;
   vector<int>     *t_fatjets_RebuildCamKt8MitruTrim_truth_label;
   Int_t           t_fatjets_RebuildAntiKt2Mitru_n;
   vector<float>   *t_fatjets_RebuildAntiKt2Mitru_pt;
   vector<float>   *t_fatjets_RebuildAntiKt2Mitru_eta;
   vector<float>   *t_fatjets_RebuildAntiKt2Mitru_phi;
   vector<float>   *t_fatjets_RebuildAntiKt2Mitru_m;
   vector<float>   *t_fatjets_RebuildAntiKt2Mitru_SPLIT12;
   vector<float>   *t_fatjets_RebuildAntiKt2Mitru_SPLIT23;
   vector<float>   *t_fatjets_RebuildAntiKt2Mitru_Tau1;
   vector<float>   *t_fatjets_RebuildAntiKt2Mitru_Tau2;
   vector<float>   *t_fatjets_RebuildAntiKt2Mitru_Tau3;
   vector<int>     *t_fatjets_RebuildAntiKt2Mitru_MCclass;
   vector<int>     *t_fatjets_RebuildAntiKt2Mitru_orig_ind;
   vector<int>     *t_fatjets_RebuildAntiKt2Mitru_assoc_ind;
   vector<int>     *t_fatjets_RebuildAntiKt2Mitru_truth_label;
   Int_t           t_fatjets_RebuildCamKt6MitruSplitFilt_n;
   vector<float>   *t_fatjets_RebuildCamKt6MitruSplitFilt_pt;
   vector<float>   *t_fatjets_RebuildCamKt6MitruSplitFilt_eta;
   vector<float>   *t_fatjets_RebuildCamKt6MitruSplitFilt_phi;
   vector<float>   *t_fatjets_RebuildCamKt6MitruSplitFilt_m;
   vector<float>   *t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12;
   vector<float>   *t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23;
   vector<float>   *t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1;
   vector<float>   *t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2;
   vector<float>   *t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3;
   vector<int>     *t_fatjets_RebuildCamKt6MitruSplitFilt_MCclass;
   vector<int>     *t_fatjets_RebuildCamKt6MitruSplitFilt_orig_ind;
   vector<int>     *t_fatjets_RebuildCamKt6MitruSplitFilt_assoc_ind;
   vector<int>     *t_fatjets_RebuildCamKt6MitruSplitFilt_truth_label;
   Int_t           t_fatjets_RebuildCamKt6MitruTrim_n;
   vector<float>   *t_fatjets_RebuildCamKt6MitruTrim_pt;
   vector<float>   *t_fatjets_RebuildCamKt6MitruTrim_eta;
   vector<float>   *t_fatjets_RebuildCamKt6MitruTrim_phi;
   vector<float>   *t_fatjets_RebuildCamKt6MitruTrim_m;
   vector<float>   *t_fatjets_RebuildCamKt6MitruTrim_SPLIT12;
   vector<float>   *t_fatjets_RebuildCamKt6MitruTrim_SPLIT23;
   vector<float>   *t_fatjets_RebuildCamKt6MitruTrim_Tau1;
   vector<float>   *t_fatjets_RebuildCamKt6MitruTrim_Tau2;
   vector<float>   *t_fatjets_RebuildCamKt6MitruTrim_Tau3;
   vector<int>     *t_fatjets_RebuildCamKt6MitruTrim_MCclass;
   vector<int>     *t_fatjets_RebuildCamKt6MitruTrim_orig_ind;
   vector<int>     *t_fatjets_RebuildCamKt6MitruTrim_assoc_ind;
   vector<int>     *t_fatjets_RebuildCamKt6MitruTrim_truth_label;
   Int_t           t_fatjets_RebuildCamKt10MitruSplitFilt_n;
   vector<float>   *t_fatjets_RebuildCamKt10MitruSplitFilt_pt;
   vector<float>   *t_fatjets_RebuildCamKt10MitruSplitFilt_eta;
   vector<float>   *t_fatjets_RebuildCamKt10MitruSplitFilt_phi;
   vector<float>   *t_fatjets_RebuildCamKt10MitruSplitFilt_m;
   vector<float>   *t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT12;
   vector<float>   *t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT23;
   vector<float>   *t_fatjets_RebuildCamKt10MitruSplitFilt_Tau1;
   vector<float>   *t_fatjets_RebuildCamKt10MitruSplitFilt_Tau2;
   vector<float>   *t_fatjets_RebuildCamKt10MitruSplitFilt_Tau3;
   vector<int>     *t_fatjets_RebuildCamKt10MitruSplitFilt_MCclass;
   vector<int>     *t_fatjets_RebuildCamKt10MitruSplitFilt_orig_ind;
   vector<int>     *t_fatjets_RebuildCamKt10MitruSplitFilt_assoc_ind;
   vector<int>     *t_fatjets_RebuildCamKt10MitruSplitFilt_truth_label;
   Int_t           t_fatjets_RebuildCamKt10MitruTrim_n;
   vector<float>   *t_fatjets_RebuildCamKt10MitruTrim_pt;
   vector<float>   *t_fatjets_RebuildCamKt10MitruTrim_eta;
   vector<float>   *t_fatjets_RebuildCamKt10MitruTrim_phi;
   vector<float>   *t_fatjets_RebuildCamKt10MitruTrim_m;
   vector<float>   *t_fatjets_RebuildCamKt10MitruTrim_SPLIT12;
   vector<float>   *t_fatjets_RebuildCamKt10MitruTrim_SPLIT23;
   vector<float>   *t_fatjets_RebuildCamKt10MitruTrim_Tau1;
   vector<float>   *t_fatjets_RebuildCamKt10MitruTrim_Tau2;
   vector<float>   *t_fatjets_RebuildCamKt10MitruTrim_Tau3;
   vector<int>     *t_fatjets_RebuildCamKt10MitruTrim_MCclass;
   vector<int>     *t_fatjets_RebuildCamKt10MitruTrim_orig_ind;
   vector<int>     *t_fatjets_RebuildCamKt10MitruTrim_assoc_ind;
   vector<int>     *t_fatjets_RebuildCamKt10MitruTrim_truth_label;
   Int_t           t_fatjets_RebuildAntiKt4Mitru_n;
   vector<float>   *t_fatjets_RebuildAntiKt4Mitru_pt;
   vector<float>   *t_fatjets_RebuildAntiKt4Mitru_eta;
   vector<float>   *t_fatjets_RebuildAntiKt4Mitru_phi;
   vector<float>   *t_fatjets_RebuildAntiKt4Mitru_m;
   vector<float>   *t_fatjets_RebuildAntiKt4Mitru_SPLIT12;
   vector<float>   *t_fatjets_RebuildAntiKt4Mitru_SPLIT23;
   vector<float>   *t_fatjets_RebuildAntiKt4Mitru_Tau1;
   vector<float>   *t_fatjets_RebuildAntiKt4Mitru_Tau2;
   vector<float>   *t_fatjets_RebuildAntiKt4Mitru_Tau3;
   vector<int>     *t_fatjets_RebuildAntiKt4Mitru_MCclass;
   vector<int>     *t_fatjets_RebuildAntiKt4Mitru_orig_ind;
   vector<int>     *t_fatjets_RebuildAntiKt4Mitru_assoc_ind;
   vector<int>     *t_fatjets_RebuildAntiKt4Mitru_truth_label;
   Double_t        t_mcevt_weight;
   Int_t           t_HF_class;

   // List of branches
   TBranch        *b_t_p_leptop_pt;   //!
   TBranch        *b_t_p_leptop_eta;   //!
   TBranch        *b_t_p_leptop_phi;   //!
   TBranch        *b_t_p_leptop_m;   //!
   TBranch        *b_t_p_leptop_hard_pt;   //!
   TBranch        *b_t_p_leptop_hard_eta;   //!
   TBranch        *b_t_p_leptop_hard_phi;   //!
   TBranch        *b_t_p_leptop_hard_m;   //!
   TBranch        *b_t_p_leptop_b_pt;   //!
   TBranch        *b_t_p_leptop_b_eta;   //!
   TBranch        *b_t_p_leptop_b_phi;   //!
   TBranch        *b_t_p_leptop_b_m;   //!
   TBranch        *b_t_p_leptop_W_pt;   //!
   TBranch        *b_t_p_leptop_W_eta;   //!
   TBranch        *b_t_p_leptop_W_phi;   //!
   TBranch        *b_t_p_leptop_W_m;   //!
   TBranch        *b_t_p_leptop_lep_pt;   //!
   TBranch        *b_t_p_leptop_lep_eta;   //!
   TBranch        *b_t_p_leptop_lep_phi;   //!
   TBranch        *b_t_p_leptop_lep_m;   //!
   TBranch        *b_t_p_leptop_lep_flav;   //!
   TBranch        *b_t_p_leptop_nu_pt;   //!
   TBranch        *b_t_p_leptop_nu_eta;   //!
   TBranch        *b_t_p_leptop_nu_phi;   //!
   TBranch        *b_t_p_leptop_nu_m;   //!
   TBranch        *b_t_p_hadtop_pt;   //!
   TBranch        *b_t_p_hadtop_eta;   //!
   TBranch        *b_t_p_hadtop_phi;   //!
   TBranch        *b_t_p_hadtop_m;   //!
   TBranch        *b_t_p_hadtop_hard_pt;   //!
   TBranch        *b_t_p_hadtop_hard_eta;   //!
   TBranch        *b_t_p_hadtop_hard_phi;   //!
   TBranch        *b_t_p_hadtop_hard_m;   //!
   TBranch        *b_t_p_hadtop_b_pt;   //!
   TBranch        *b_t_p_hadtop_b_eta;   //!
   TBranch        *b_t_p_hadtop_b_phi;   //!
   TBranch        *b_t_p_hadtop_b_m;   //!
   TBranch        *b_t_p_hadtop_W_pt;   //!
   TBranch        *b_t_p_hadtop_W_eta;   //!
   TBranch        *b_t_p_hadtop_W_phi;   //!
   TBranch        *b_t_p_hadtop_W_m;   //!
   TBranch        *b_t_p_hadtop_q1_pt;   //!
   TBranch        *b_t_p_hadtop_q1_eta;   //!
   TBranch        *b_t_p_hadtop_q1_phi;   //!
   TBranch        *b_t_p_hadtop_q1_m;   //!
   TBranch        *b_t_p_hadtop_q1_flav;   //!
   TBranch        *b_t_p_hadtop_q2_pt;   //!
   TBranch        *b_t_p_hadtop_q2_eta;   //!
   TBranch        *b_t_p_hadtop_q2_phi;   //!
   TBranch        *b_t_p_hadtop_q2_m;   //!
   TBranch        *b_t_p_hadtop_q2_flav;   //!
   TBranch        *b_t_p_higgs_pt;   //!
   TBranch        *b_t_p_higgs_eta;   //!
   TBranch        *b_t_p_higgs_phi;   //!
   TBranch        *b_t_p_higgs_m;   //!
   TBranch        *b_t_p_higgs_hard_pt;   //!
   TBranch        *b_t_p_higgs_hard_eta;   //!
   TBranch        *b_t_p_higgs_hard_phi;   //!
   TBranch        *b_t_p_higgs_hard_m;   //!
   TBranch        *b_t_p_higgs_b1_pt;   //!
   TBranch        *b_t_p_higgs_b1_eta;   //!
   TBranch        *b_t_p_higgs_b1_phi;   //!
   TBranch        *b_t_p_higgs_b1_m;   //!
   TBranch        *b_t_p_higgs_b2_pt;   //!
   TBranch        *b_t_p_higgs_b2_eta;   //!
   TBranch        *b_t_p_higgs_b2_phi;   //!
   TBranch        *b_t_p_higgs_b2_m;   //!
   TBranch        *b_t_j_leptop_b_ind;   //!
   TBranch        *b_t_pj_leptop_b_dr;   //!
   TBranch        *b_t_j_leptop_lep_pt;   //!
   TBranch        *b_t_j_leptop_lep_eta;   //!
   TBranch        *b_t_j_leptop_lep_phi;   //!
   TBranch        *b_t_j_leptop_lep_m;   //!
   TBranch        *b_t_j_leptop_lep_flav;   //!
   TBranch        *b_t_j_leptop_lep_charge;   //!
   TBranch        *b_t_j_leptop_nu_pt;   //!
   TBranch        *b_t_j_leptop_nu_phi;   //!
   TBranch        *b_t_pj_leptop_lep_dr;   //!
   TBranch        *b_t_pj_leptop_nu_dphi;   //!
   TBranch        *b_t_j_hadtop_b_ind;   //!
   TBranch        *b_t_pj_hadtop_b_dr;   //!
   TBranch        *b_t_j_hadtop_q1_ind;   //!
   TBranch        *b_t_pj_hadtop_q1_dr;   //!
   TBranch        *b_t_j_hadtop_q2_ind;   //!
   TBranch        *b_t_pj_hadtop_q2_dr;   //!
   TBranch        *b_t_j_higgs_b1_ind;   //!
   TBranch        *b_t_pj_higgs_b1_dr;   //!
   TBranch        *b_t_j_higgs_b2_ind;   //!
   TBranch        *b_t_pj_higgs_b2_dr;   //!
   TBranch        *b_t_jets_pt;   //!
   TBranch        *b_t_jets_eta;   //!
   TBranch        *b_t_jets_phi;   //!
   TBranch        *b_t_jets_m;   //!
   TBranch        *b_t_jets_MV1;   //!
   TBranch        *b_t_jets_trueflavor;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_n;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_pt;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_eta;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_phi;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_m;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT12;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT23;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_Tau3;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_MCclass;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_orig_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_assoc_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruSplitFilt_truth_label;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_n;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_pt;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_eta;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_phi;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_m;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_SPLIT12;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_SPLIT23;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_Tau1;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_Tau2;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_Tau3;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_MCclass;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_orig_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_assoc_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt8MitruTrim_truth_label;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_n;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_pt;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_eta;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_phi;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_m;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_SPLIT12;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_SPLIT23;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_Tau1;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_Tau2;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_Tau3;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_MCclass;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_orig_ind;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_assoc_ind;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt2Mitru_truth_label;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_n;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_pt;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_eta;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_phi;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_m;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_MCclass;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_orig_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_assoc_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruSplitFilt_truth_label;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_n;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_pt;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_eta;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_phi;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_m;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_SPLIT12;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_SPLIT23;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_Tau1;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_Tau2;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_Tau3;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_MCclass;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_orig_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_assoc_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt6MitruTrim_truth_label;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_n;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_pt;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_eta;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_phi;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_m;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT12;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT23;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_Tau1;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_Tau2;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_Tau3;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_MCclass;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_orig_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_assoc_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruSplitFilt_truth_label;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_n;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_pt;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_eta;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_phi;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_m;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_SPLIT12;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_SPLIT23;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_Tau1;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_Tau2;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_Tau3;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_MCclass;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_orig_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_assoc_ind;   //!
   TBranch        *b_t_fatjets_RebuildCamKt10MitruTrim_truth_label;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_n;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_pt;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_eta;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_phi;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_m;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_SPLIT12;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_SPLIT23;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_Tau1;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_Tau2;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_Tau3;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_MCclass;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_orig_ind;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_assoc_ind;   //!
   TBranch        *b_t_fatjets_RebuildAntiKt4Mitru_truth_label;   //!
   TBranch        *b_t_mcevt_weight;   //!
   TBranch        *b_t_HF_class;   //!

   Reader(TChain *tree);
   virtual ~Reader();
   virtual void     Init(TChain *tree);
   virtual void     Loop();
   string m_ds_name;
   int m_ds_num;
   int m_ini_num;
   int m_fin_num;
   bool m_doTRF;
   bool m_doSys; 
   string m_out_folder;
   string m_suffix_file;
   void SetSuffix(string value){m_suffix_file=value;};
   void SetDS(string value){m_ds_name=value;};
   void SetTRF(bool value){m_doTRF=value;};
   void SetSys(bool value){m_doSys=value;};
   void SetOutFold(string value){m_out_folder=value;};
   void SetSSEvent(int value1,int value2){m_ini_num=value1;m_fin_num=value2;};

   int m_mass;
   float m_pt_mass;
   void SetMass(int value){m_mass=value;};

   Int_t           m_fatjets_n;
   vector<float>   m_fatjets_pt;
   vector<float>   m_fatjets_eta;
   vector<float>   m_fatjets_phi;
   vector<float>   m_fatjets_m;
   /*
   vector<float>   m_fatjets_SPLIT12;
   vector<float>   m_fatjets_SPLIT23;
   vector<float>   m_fatjets_Tau1;
   vector<float>   m_fatjets_Tau2;
   vector<float>   m_fatjets_Tau3;
   vector<int>     m_fatjets_MCclass;
   vector<int>     m_fatjets_orig_ind;
   vector<int>     m_fatjets_assoc_ind;
   vector<int>     m_fatjets_truth_label;
   */

  



   int m_nj_min;
   int m_nj_max;
   int m_enj_max;
   int m_nb_min;
   int m_nb_max;
   int m_enb_max;
   void SetNJets(int value1,int value2,int value3,int value4){m_nj_min=value1;m_nj_max=value2;m_nb_min=value3;m_nb_max=value4;};

   void Begin();
   void Terminate();

   float GetHTall();
    
   void BookTH1D(string name, int nbins, double xlow, double xup, const char* title="", const char* xtitle="", const char* ytitle="", int lw=1, int lc=1);
   void BookTH1D(string name, int nbins, double* xedges, const char* title="" ,const char* xtitle="", const char* ytitle="", int lw=1, int lc=1);
   void FillTH1D(string hname, double val, double weight=1.);

   map<string,TFile *> m_output;
   void BookOutput(string name);
   vector<string> sample_titles;
  
   vector<string> *histo_names;
   void FillHistoNames();

   
   vector<string> *m_syst;
   void FillSystNames();


   TopTruthReweighting *m_truthreweighter;

   void DoJetFatMatch(vector<int> &match_idx,vector<float> *pt,vector<float> *eta,vector<float> *phi,vector<float> *m,float DR,vector<float> *pt_ref,vector<float> *eta_ref,vector<float> *phi_ref,vector<float> *m_ref);

   int GetFatJetBContentTRF(int fatjet_index, vector<int> match_idx,vector<bool> permutation);
   int GetFatJetContent(int fatjet_index, vector<int> match_idx);

   float GetAntiKtMass(int fatjet_index,vector<int> match_idx);
   float GetAntiKtDR(int fatjet_index,vector<int> match_idx,vector<bool> permutation);
   float GetFatDR(int fatjet_index,vector<int> match_idx);
   
   int GetAntiKtMultiplicity(int fatjet_index,vector<int> match_idx,vector<int> &selected_idx);
   void DoJetTruthMatch(vector<int> &match_idx,vector<float> *pt,vector<float> *eta,vector<float> *phi,vector<float> *m,float DR,vector<float> pt_ref,vector<float> eta_ref,vector<float> phi_ref,vector<float> m_ref,vector<int> excl_idx);
   ////////////////////

   ////HISTOS/////
   map<string,TH1D *> m_h1d;
  
   TH2F *hist_2d_HT_higgs;
   TH2F *hist_2d_HT_top;
   TH2F *hist_2d_ung_fatjet_higgs;
   TH2F *hist_2d_ung_fatjet_top;
   TH2F *hist_2d_ung_fatjetsum_higgs;
   TH2F *hist_2d_ung_fatjetsum_top;
   TH2F *hist_2d_bdrs_fatjet_higgs;
   TH2F *hist_2d_bdrs_fatjet_top;
   TH2F *hist_2d_bdrs_fatjetsum_higgs;
   TH2F *hist_2d_bdrs_fatjetsum_top;


};

#endif

#ifdef Reader_cxx
Reader::Reader(TChain *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
 
   Init(tree);
}

Reader::~Reader()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}



void Reader::Init(TChain *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer

   t_jets_pt = 0;
   t_jets_eta = 0;
   t_jets_phi = 0;
   t_jets_m = 0;
   t_jets_MV1 = 0;
   t_jets_trueflavor = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_pt = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_eta = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_phi = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_m = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT12 = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT23 = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1 = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2 = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_Tau3 = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_MCclass = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_orig_ind = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_assoc_ind = 0;
   t_fatjets_RebuildCamKt8MitruSplitFilt_truth_label = 0;
   t_fatjets_RebuildCamKt8MitruTrim_pt = 0;
   t_fatjets_RebuildCamKt8MitruTrim_eta = 0;
   t_fatjets_RebuildCamKt8MitruTrim_phi = 0;
   t_fatjets_RebuildCamKt8MitruTrim_m = 0;
   t_fatjets_RebuildCamKt8MitruTrim_SPLIT12 = 0;
   t_fatjets_RebuildCamKt8MitruTrim_SPLIT23 = 0;
   t_fatjets_RebuildCamKt8MitruTrim_Tau1 = 0;
   t_fatjets_RebuildCamKt8MitruTrim_Tau2 = 0;
   t_fatjets_RebuildCamKt8MitruTrim_Tau3 = 0;
   t_fatjets_RebuildCamKt8MitruTrim_MCclass = 0;
   t_fatjets_RebuildCamKt8MitruTrim_orig_ind = 0;
   t_fatjets_RebuildCamKt8MitruTrim_assoc_ind = 0;
   t_fatjets_RebuildCamKt8MitruTrim_truth_label = 0;
   t_fatjets_RebuildAntiKt2Mitru_pt = 0;
   t_fatjets_RebuildAntiKt2Mitru_eta = 0;
   t_fatjets_RebuildAntiKt2Mitru_phi = 0;
   t_fatjets_RebuildAntiKt2Mitru_m = 0;
   t_fatjets_RebuildAntiKt2Mitru_SPLIT12 = 0;
   t_fatjets_RebuildAntiKt2Mitru_SPLIT23 = 0;
   t_fatjets_RebuildAntiKt2Mitru_Tau1 = 0;
   t_fatjets_RebuildAntiKt2Mitru_Tau2 = 0;
   t_fatjets_RebuildAntiKt2Mitru_Tau3 = 0;
   t_fatjets_RebuildAntiKt2Mitru_MCclass = 0;
   t_fatjets_RebuildAntiKt2Mitru_orig_ind = 0;
   t_fatjets_RebuildAntiKt2Mitru_assoc_ind = 0;
   t_fatjets_RebuildAntiKt2Mitru_truth_label = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_pt = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_eta = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_phi = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_m = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12 = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23 = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1 = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2 = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3 = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_MCclass = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_orig_ind = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_assoc_ind = 0;
   t_fatjets_RebuildCamKt6MitruSplitFilt_truth_label = 0;
   t_fatjets_RebuildCamKt6MitruTrim_pt = 0;
   t_fatjets_RebuildCamKt6MitruTrim_eta = 0;
   t_fatjets_RebuildCamKt6MitruTrim_phi = 0;
   t_fatjets_RebuildCamKt6MitruTrim_m = 0;
   t_fatjets_RebuildCamKt6MitruTrim_SPLIT12 = 0;
   t_fatjets_RebuildCamKt6MitruTrim_SPLIT23 = 0;
   t_fatjets_RebuildCamKt6MitruTrim_Tau1 = 0;
   t_fatjets_RebuildCamKt6MitruTrim_Tau2 = 0;
   t_fatjets_RebuildCamKt6MitruTrim_Tau3 = 0;
   t_fatjets_RebuildCamKt6MitruTrim_MCclass = 0;
   t_fatjets_RebuildCamKt6MitruTrim_orig_ind = 0;
   t_fatjets_RebuildCamKt6MitruTrim_assoc_ind = 0;
   t_fatjets_RebuildCamKt6MitruTrim_truth_label = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_pt = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_eta = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_phi = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_m = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT12 = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT23 = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_Tau1 = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_Tau2 = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_Tau3 = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_MCclass = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_orig_ind = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_assoc_ind = 0;
   t_fatjets_RebuildCamKt10MitruSplitFilt_truth_label = 0;
   t_fatjets_RebuildCamKt10MitruTrim_pt = 0;
   t_fatjets_RebuildCamKt10MitruTrim_eta = 0;
   t_fatjets_RebuildCamKt10MitruTrim_phi = 0;
   t_fatjets_RebuildCamKt10MitruTrim_m = 0;
   t_fatjets_RebuildCamKt10MitruTrim_SPLIT12 = 0;
   t_fatjets_RebuildCamKt10MitruTrim_SPLIT23 = 0;
   t_fatjets_RebuildCamKt10MitruTrim_Tau1 = 0;
   t_fatjets_RebuildCamKt10MitruTrim_Tau2 = 0;
   t_fatjets_RebuildCamKt10MitruTrim_Tau3 = 0;
   t_fatjets_RebuildCamKt10MitruTrim_MCclass = 0;
   t_fatjets_RebuildCamKt10MitruTrim_orig_ind = 0;
   t_fatjets_RebuildCamKt10MitruTrim_assoc_ind = 0;
   t_fatjets_RebuildCamKt10MitruTrim_truth_label = 0;
   t_fatjets_RebuildAntiKt4Mitru_pt = 0;
   t_fatjets_RebuildAntiKt4Mitru_eta = 0;
   t_fatjets_RebuildAntiKt4Mitru_phi = 0;
   t_fatjets_RebuildAntiKt4Mitru_m = 0;
   t_fatjets_RebuildAntiKt4Mitru_SPLIT12 = 0;
   t_fatjets_RebuildAntiKt4Mitru_SPLIT23 = 0;
   t_fatjets_RebuildAntiKt4Mitru_Tau1 = 0;
   t_fatjets_RebuildAntiKt4Mitru_Tau2 = 0;
   t_fatjets_RebuildAntiKt4Mitru_Tau3 = 0;
   t_fatjets_RebuildAntiKt4Mitru_MCclass = 0;
   t_fatjets_RebuildAntiKt4Mitru_orig_ind = 0;
   t_fatjets_RebuildAntiKt4Mitru_assoc_ind = 0;
   t_fatjets_RebuildAntiKt4Mitru_truth_label = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("t_p_leptop_pt", &t_p_leptop_pt, &b_t_p_leptop_pt);
   fChain->SetBranchAddress("t_p_leptop_eta", &t_p_leptop_eta, &b_t_p_leptop_eta);
   fChain->SetBranchAddress("t_p_leptop_phi", &t_p_leptop_phi, &b_t_p_leptop_phi);
   fChain->SetBranchAddress("t_p_leptop_m", &t_p_leptop_m, &b_t_p_leptop_m);
   fChain->SetBranchAddress("t_p_leptop_hard_pt", &t_p_leptop_hard_pt, &b_t_p_leptop_hard_pt);
   fChain->SetBranchAddress("t_p_leptop_hard_eta", &t_p_leptop_hard_eta, &b_t_p_leptop_hard_eta);
   fChain->SetBranchAddress("t_p_leptop_hard_phi", &t_p_leptop_hard_phi, &b_t_p_leptop_hard_phi);
   fChain->SetBranchAddress("t_p_leptop_hard_m", &t_p_leptop_hard_m, &b_t_p_leptop_hard_m);
   fChain->SetBranchAddress("t_p_leptop_b_pt", &t_p_leptop_b_pt, &b_t_p_leptop_b_pt);
   fChain->SetBranchAddress("t_p_leptop_b_eta", &t_p_leptop_b_eta, &b_t_p_leptop_b_eta);
   fChain->SetBranchAddress("t_p_leptop_b_phi", &t_p_leptop_b_phi, &b_t_p_leptop_b_phi);
   fChain->SetBranchAddress("t_p_leptop_b_m", &t_p_leptop_b_m, &b_t_p_leptop_b_m);
   fChain->SetBranchAddress("t_p_leptop_W_pt", &t_p_leptop_W_pt, &b_t_p_leptop_W_pt);
   fChain->SetBranchAddress("t_p_leptop_W_eta", &t_p_leptop_W_eta, &b_t_p_leptop_W_eta);
   fChain->SetBranchAddress("t_p_leptop_W_phi", &t_p_leptop_W_phi, &b_t_p_leptop_W_phi);
   fChain->SetBranchAddress("t_p_leptop_W_m", &t_p_leptop_W_m, &b_t_p_leptop_W_m);
   fChain->SetBranchAddress("t_p_leptop_lep_pt", &t_p_leptop_lep_pt, &b_t_p_leptop_lep_pt);
   fChain->SetBranchAddress("t_p_leptop_lep_eta", &t_p_leptop_lep_eta, &b_t_p_leptop_lep_eta);
   fChain->SetBranchAddress("t_p_leptop_lep_phi", &t_p_leptop_lep_phi, &b_t_p_leptop_lep_phi);
   fChain->SetBranchAddress("t_p_leptop_lep_m", &t_p_leptop_lep_m, &b_t_p_leptop_lep_m);
   fChain->SetBranchAddress("t_p_leptop_lep_flav", &t_p_leptop_lep_flav, &b_t_p_leptop_lep_flav);
   fChain->SetBranchAddress("t_p_leptop_nu_pt", &t_p_leptop_nu_pt, &b_t_p_leptop_nu_pt);
   fChain->SetBranchAddress("t_p_leptop_nu_eta", &t_p_leptop_nu_eta, &b_t_p_leptop_nu_eta);
   fChain->SetBranchAddress("t_p_leptop_nu_phi", &t_p_leptop_nu_phi, &b_t_p_leptop_nu_phi);
   fChain->SetBranchAddress("t_p_leptop_nu_m", &t_p_leptop_nu_m, &b_t_p_leptop_nu_m);
   fChain->SetBranchAddress("t_p_hadtop_pt", &t_p_hadtop_pt, &b_t_p_hadtop_pt);
   fChain->SetBranchAddress("t_p_hadtop_eta", &t_p_hadtop_eta, &b_t_p_hadtop_eta);
   fChain->SetBranchAddress("t_p_hadtop_phi", &t_p_hadtop_phi, &b_t_p_hadtop_phi);
   fChain->SetBranchAddress("t_p_hadtop_m", &t_p_hadtop_m, &b_t_p_hadtop_m);
   fChain->SetBranchAddress("t_p_hadtop_hard_pt", &t_p_hadtop_hard_pt, &b_t_p_hadtop_hard_pt);
   fChain->SetBranchAddress("t_p_hadtop_hard_eta", &t_p_hadtop_hard_eta, &b_t_p_hadtop_hard_eta);
   fChain->SetBranchAddress("t_p_hadtop_hard_phi", &t_p_hadtop_hard_phi, &b_t_p_hadtop_hard_phi);
   fChain->SetBranchAddress("t_p_hadtop_hard_m", &t_p_hadtop_hard_m, &b_t_p_hadtop_hard_m);
   fChain->SetBranchAddress("t_p_hadtop_b_pt", &t_p_hadtop_b_pt, &b_t_p_hadtop_b_pt);
   fChain->SetBranchAddress("t_p_hadtop_b_eta", &t_p_hadtop_b_eta, &b_t_p_hadtop_b_eta);
   fChain->SetBranchAddress("t_p_hadtop_b_phi", &t_p_hadtop_b_phi, &b_t_p_hadtop_b_phi);
   fChain->SetBranchAddress("t_p_hadtop_b_m", &t_p_hadtop_b_m, &b_t_p_hadtop_b_m);
   fChain->SetBranchAddress("t_p_hadtop_W_pt", &t_p_hadtop_W_pt, &b_t_p_hadtop_W_pt);
   fChain->SetBranchAddress("t_p_hadtop_W_eta", &t_p_hadtop_W_eta, &b_t_p_hadtop_W_eta);
   fChain->SetBranchAddress("t_p_hadtop_W_phi", &t_p_hadtop_W_phi, &b_t_p_hadtop_W_phi);
   fChain->SetBranchAddress("t_p_hadtop_W_m", &t_p_hadtop_W_m, &b_t_p_hadtop_W_m);
   fChain->SetBranchAddress("t_p_hadtop_q1_pt", &t_p_hadtop_q1_pt, &b_t_p_hadtop_q1_pt);
   fChain->SetBranchAddress("t_p_hadtop_q1_eta", &t_p_hadtop_q1_eta, &b_t_p_hadtop_q1_eta);
   fChain->SetBranchAddress("t_p_hadtop_q1_phi", &t_p_hadtop_q1_phi, &b_t_p_hadtop_q1_phi);
   fChain->SetBranchAddress("t_p_hadtop_q1_m", &t_p_hadtop_q1_m, &b_t_p_hadtop_q1_m);
   fChain->SetBranchAddress("t_p_hadtop_q1_flav", &t_p_hadtop_q1_flav, &b_t_p_hadtop_q1_flav);
   fChain->SetBranchAddress("t_p_hadtop_q2_pt", &t_p_hadtop_q2_pt, &b_t_p_hadtop_q2_pt);
   fChain->SetBranchAddress("t_p_hadtop_q2_eta", &t_p_hadtop_q2_eta, &b_t_p_hadtop_q2_eta);
   fChain->SetBranchAddress("t_p_hadtop_q2_phi", &t_p_hadtop_q2_phi, &b_t_p_hadtop_q2_phi);
   fChain->SetBranchAddress("t_p_hadtop_q2_m", &t_p_hadtop_q2_m, &b_t_p_hadtop_q2_m);
   fChain->SetBranchAddress("t_p_hadtop_q2_flav", &t_p_hadtop_q2_flav, &b_t_p_hadtop_q2_flav);
   fChain->SetBranchAddress("t_p_higgs_pt", &t_p_higgs_pt, &b_t_p_higgs_pt);
   fChain->SetBranchAddress("t_p_higgs_eta", &t_p_higgs_eta, &b_t_p_higgs_eta);
   fChain->SetBranchAddress("t_p_higgs_phi", &t_p_higgs_phi, &b_t_p_higgs_phi);
   fChain->SetBranchAddress("t_p_higgs_m", &t_p_higgs_m, &b_t_p_higgs_m);
   fChain->SetBranchAddress("t_p_higgs_hard_pt", &t_p_higgs_hard_pt, &b_t_p_higgs_hard_pt);
   fChain->SetBranchAddress("t_p_higgs_hard_eta", &t_p_higgs_hard_eta, &b_t_p_higgs_hard_eta);
   fChain->SetBranchAddress("t_p_higgs_hard_phi", &t_p_higgs_hard_phi, &b_t_p_higgs_hard_phi);
   fChain->SetBranchAddress("t_p_higgs_hard_m", &t_p_higgs_hard_m, &b_t_p_higgs_hard_m);
   fChain->SetBranchAddress("t_p_higgs_b1_pt", &t_p_higgs_b1_pt, &b_t_p_higgs_b1_pt);
   fChain->SetBranchAddress("t_p_higgs_b1_eta", &t_p_higgs_b1_eta, &b_t_p_higgs_b1_eta);
   fChain->SetBranchAddress("t_p_higgs_b1_phi", &t_p_higgs_b1_phi, &b_t_p_higgs_b1_phi);
   fChain->SetBranchAddress("t_p_higgs_b1_m", &t_p_higgs_b1_m, &b_t_p_higgs_b1_m);
   fChain->SetBranchAddress("t_p_higgs_b2_pt", &t_p_higgs_b2_pt, &b_t_p_higgs_b2_pt);
   fChain->SetBranchAddress("t_p_higgs_b2_eta", &t_p_higgs_b2_eta, &b_t_p_higgs_b2_eta);
   fChain->SetBranchAddress("t_p_higgs_b2_phi", &t_p_higgs_b2_phi, &b_t_p_higgs_b2_phi);
   fChain->SetBranchAddress("t_p_higgs_b2_m", &t_p_higgs_b2_m, &b_t_p_higgs_b2_m);
   fChain->SetBranchAddress("t_j_leptop_b_ind", &t_j_leptop_b_ind, &b_t_j_leptop_b_ind);
   fChain->SetBranchAddress("t_pj_leptop_b_dr", &t_pj_leptop_b_dr, &b_t_pj_leptop_b_dr);
   fChain->SetBranchAddress("t_j_leptop_lep_pt", &t_j_leptop_lep_pt, &b_t_j_leptop_lep_pt);
   fChain->SetBranchAddress("t_j_leptop_lep_eta", &t_j_leptop_lep_eta, &b_t_j_leptop_lep_eta);
   fChain->SetBranchAddress("t_j_leptop_lep_phi", &t_j_leptop_lep_phi, &b_t_j_leptop_lep_phi);
   fChain->SetBranchAddress("t_j_leptop_lep_m", &t_j_leptop_lep_m, &b_t_j_leptop_lep_m);
   fChain->SetBranchAddress("t_j_leptop_lep_flav", &t_j_leptop_lep_flav, &b_t_j_leptop_lep_flav);
   fChain->SetBranchAddress("t_j_leptop_lep_charge", &t_j_leptop_lep_charge, &b_t_j_leptop_lep_charge);
   fChain->SetBranchAddress("t_j_leptop_nu_pt", &t_j_leptop_nu_pt, &b_t_j_leptop_nu_pt);
   fChain->SetBranchAddress("t_j_leptop_nu_phi", &t_j_leptop_nu_phi, &b_t_j_leptop_nu_phi);
   fChain->SetBranchAddress("t_pj_leptop_lep_dr", &t_pj_leptop_lep_dr, &b_t_pj_leptop_lep_dr);
   fChain->SetBranchAddress("t_pj_leptop_nu_dphi", &t_pj_leptop_nu_dphi, &b_t_pj_leptop_nu_dphi);
   fChain->SetBranchAddress("t_j_hadtop_b_ind", &t_j_hadtop_b_ind, &b_t_j_hadtop_b_ind);
   fChain->SetBranchAddress("t_pj_hadtop_b_dr", &t_pj_hadtop_b_dr, &b_t_pj_hadtop_b_dr);
   fChain->SetBranchAddress("t_j_hadtop_q1_ind", &t_j_hadtop_q1_ind, &b_t_j_hadtop_q1_ind);
   fChain->SetBranchAddress("t_pj_hadtop_q1_dr", &t_pj_hadtop_q1_dr, &b_t_pj_hadtop_q1_dr);
   fChain->SetBranchAddress("t_j_hadtop_q2_ind", &t_j_hadtop_q2_ind, &b_t_j_hadtop_q2_ind);
   fChain->SetBranchAddress("t_pj_hadtop_q2_dr", &t_pj_hadtop_q2_dr, &b_t_pj_hadtop_q2_dr);
   fChain->SetBranchAddress("t_j_higgs_b1_ind", &t_j_higgs_b1_ind, &b_t_j_higgs_b1_ind);
   fChain->SetBranchAddress("t_pj_higgs_b1_dr", &t_pj_higgs_b1_dr, &b_t_pj_higgs_b1_dr);
   fChain->SetBranchAddress("t_j_higgs_b2_ind", &t_j_higgs_b2_ind, &b_t_j_higgs_b2_ind);
   fChain->SetBranchAddress("t_pj_higgs_b2_dr", &t_pj_higgs_b2_dr, &b_t_pj_higgs_b2_dr);
   fChain->SetBranchAddress("t_jets_pt", &t_jets_pt, &b_t_jets_pt);
   fChain->SetBranchAddress("t_jets_eta", &t_jets_eta, &b_t_jets_eta);
   fChain->SetBranchAddress("t_jets_phi", &t_jets_phi, &b_t_jets_phi);
   fChain->SetBranchAddress("t_jets_m", &t_jets_m, &b_t_jets_m);
   fChain->SetBranchAddress("t_jets_MV1", &t_jets_MV1, &b_t_jets_MV1);
   fChain->SetBranchAddress("t_jets_trueflavor", &t_jets_trueflavor, &b_t_jets_trueflavor);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_n", &t_fatjets_RebuildCamKt8MitruSplitFilt_n, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_n);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_pt", &t_fatjets_RebuildCamKt8MitruSplitFilt_pt, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_pt);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_eta", &t_fatjets_RebuildCamKt8MitruSplitFilt_eta, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_eta);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_phi", &t_fatjets_RebuildCamKt8MitruSplitFilt_phi, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_phi);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_m", &t_fatjets_RebuildCamKt8MitruSplitFilt_m, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_m);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT12", &t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT12, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT12);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT23", &t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT23, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_SPLIT23);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1", &t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_Tau1);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2", &t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_Tau2);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_Tau3", &t_fatjets_RebuildCamKt8MitruSplitFilt_Tau3, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_Tau3);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_MCclass", &t_fatjets_RebuildCamKt8MitruSplitFilt_MCclass, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_MCclass);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_orig_ind", &t_fatjets_RebuildCamKt8MitruSplitFilt_orig_ind, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_orig_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_assoc_ind", &t_fatjets_RebuildCamKt8MitruSplitFilt_assoc_ind, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_assoc_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruSplitFilt_truth_label", &t_fatjets_RebuildCamKt8MitruSplitFilt_truth_label, &b_t_fatjets_RebuildCamKt8MitruSplitFilt_truth_label);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_n", &t_fatjets_RebuildCamKt8MitruTrim_n, &b_t_fatjets_RebuildCamKt8MitruTrim_n);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_pt", &t_fatjets_RebuildCamKt8MitruTrim_pt, &b_t_fatjets_RebuildCamKt8MitruTrim_pt);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_eta", &t_fatjets_RebuildCamKt8MitruTrim_eta, &b_t_fatjets_RebuildCamKt8MitruTrim_eta);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_phi", &t_fatjets_RebuildCamKt8MitruTrim_phi, &b_t_fatjets_RebuildCamKt8MitruTrim_phi);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_m", &t_fatjets_RebuildCamKt8MitruTrim_m, &b_t_fatjets_RebuildCamKt8MitruTrim_m);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_SPLIT12", &t_fatjets_RebuildCamKt8MitruTrim_SPLIT12, &b_t_fatjets_RebuildCamKt8MitruTrim_SPLIT12);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_SPLIT23", &t_fatjets_RebuildCamKt8MitruTrim_SPLIT23, &b_t_fatjets_RebuildCamKt8MitruTrim_SPLIT23);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_Tau1", &t_fatjets_RebuildCamKt8MitruTrim_Tau1, &b_t_fatjets_RebuildCamKt8MitruTrim_Tau1);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_Tau2", &t_fatjets_RebuildCamKt8MitruTrim_Tau2, &b_t_fatjets_RebuildCamKt8MitruTrim_Tau2);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_Tau3", &t_fatjets_RebuildCamKt8MitruTrim_Tau3, &b_t_fatjets_RebuildCamKt8MitruTrim_Tau3);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_MCclass", &t_fatjets_RebuildCamKt8MitruTrim_MCclass, &b_t_fatjets_RebuildCamKt8MitruTrim_MCclass);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_orig_ind", &t_fatjets_RebuildCamKt8MitruTrim_orig_ind, &b_t_fatjets_RebuildCamKt8MitruTrim_orig_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_assoc_ind", &t_fatjets_RebuildCamKt8MitruTrim_assoc_ind, &b_t_fatjets_RebuildCamKt8MitruTrim_assoc_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt8MitruTrim_truth_label", &t_fatjets_RebuildCamKt8MitruTrim_truth_label, &b_t_fatjets_RebuildCamKt8MitruTrim_truth_label);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_n", &t_fatjets_RebuildAntiKt2Mitru_n, &b_t_fatjets_RebuildAntiKt2Mitru_n);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_pt", &t_fatjets_RebuildAntiKt2Mitru_pt, &b_t_fatjets_RebuildAntiKt2Mitru_pt);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_eta", &t_fatjets_RebuildAntiKt2Mitru_eta, &b_t_fatjets_RebuildAntiKt2Mitru_eta);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_phi", &t_fatjets_RebuildAntiKt2Mitru_phi, &b_t_fatjets_RebuildAntiKt2Mitru_phi);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_m", &t_fatjets_RebuildAntiKt2Mitru_m, &b_t_fatjets_RebuildAntiKt2Mitru_m);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_SPLIT12", &t_fatjets_RebuildAntiKt2Mitru_SPLIT12, &b_t_fatjets_RebuildAntiKt2Mitru_SPLIT12);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_SPLIT23", &t_fatjets_RebuildAntiKt2Mitru_SPLIT23, &b_t_fatjets_RebuildAntiKt2Mitru_SPLIT23);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_Tau1", &t_fatjets_RebuildAntiKt2Mitru_Tau1, &b_t_fatjets_RebuildAntiKt2Mitru_Tau1);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_Tau2", &t_fatjets_RebuildAntiKt2Mitru_Tau2, &b_t_fatjets_RebuildAntiKt2Mitru_Tau2);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_Tau3", &t_fatjets_RebuildAntiKt2Mitru_Tau3, &b_t_fatjets_RebuildAntiKt2Mitru_Tau3);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_MCclass", &t_fatjets_RebuildAntiKt2Mitru_MCclass, &b_t_fatjets_RebuildAntiKt2Mitru_MCclass);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_orig_ind", &t_fatjets_RebuildAntiKt2Mitru_orig_ind, &b_t_fatjets_RebuildAntiKt2Mitru_orig_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_assoc_ind", &t_fatjets_RebuildAntiKt2Mitru_assoc_ind, &b_t_fatjets_RebuildAntiKt2Mitru_assoc_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt2Mitru_truth_label", &t_fatjets_RebuildAntiKt2Mitru_truth_label, &b_t_fatjets_RebuildAntiKt2Mitru_truth_label);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_n", &t_fatjets_RebuildCamKt6MitruSplitFilt_n, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_n);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_pt", &t_fatjets_RebuildCamKt6MitruSplitFilt_pt, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_pt);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_eta", &t_fatjets_RebuildCamKt6MitruSplitFilt_eta, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_eta);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_phi", &t_fatjets_RebuildCamKt6MitruSplitFilt_phi, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_phi);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_m", &t_fatjets_RebuildCamKt6MitruSplitFilt_m, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_m);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12", &t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT12);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23", &t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_SPLIT23);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1", &t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_Tau1);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2", &t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_Tau2);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3", &t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_Tau3);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_MCclass", &t_fatjets_RebuildCamKt6MitruSplitFilt_MCclass, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_MCclass);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_orig_ind", &t_fatjets_RebuildCamKt6MitruSplitFilt_orig_ind, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_orig_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_assoc_ind", &t_fatjets_RebuildCamKt6MitruSplitFilt_assoc_ind, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_assoc_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruSplitFilt_truth_label", &t_fatjets_RebuildCamKt6MitruSplitFilt_truth_label, &b_t_fatjets_RebuildCamKt6MitruSplitFilt_truth_label);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_n", &t_fatjets_RebuildCamKt6MitruTrim_n, &b_t_fatjets_RebuildCamKt6MitruTrim_n);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_pt", &t_fatjets_RebuildCamKt6MitruTrim_pt, &b_t_fatjets_RebuildCamKt6MitruTrim_pt);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_eta", &t_fatjets_RebuildCamKt6MitruTrim_eta, &b_t_fatjets_RebuildCamKt6MitruTrim_eta);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_phi", &t_fatjets_RebuildCamKt6MitruTrim_phi, &b_t_fatjets_RebuildCamKt6MitruTrim_phi);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_m", &t_fatjets_RebuildCamKt6MitruTrim_m, &b_t_fatjets_RebuildCamKt6MitruTrim_m);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_SPLIT12", &t_fatjets_RebuildCamKt6MitruTrim_SPLIT12, &b_t_fatjets_RebuildCamKt6MitruTrim_SPLIT12);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_SPLIT23", &t_fatjets_RebuildCamKt6MitruTrim_SPLIT23, &b_t_fatjets_RebuildCamKt6MitruTrim_SPLIT23);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_Tau1", &t_fatjets_RebuildCamKt6MitruTrim_Tau1, &b_t_fatjets_RebuildCamKt6MitruTrim_Tau1);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_Tau2", &t_fatjets_RebuildCamKt6MitruTrim_Tau2, &b_t_fatjets_RebuildCamKt6MitruTrim_Tau2);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_Tau3", &t_fatjets_RebuildCamKt6MitruTrim_Tau3, &b_t_fatjets_RebuildCamKt6MitruTrim_Tau3);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_MCclass", &t_fatjets_RebuildCamKt6MitruTrim_MCclass, &b_t_fatjets_RebuildCamKt6MitruTrim_MCclass);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_orig_ind", &t_fatjets_RebuildCamKt6MitruTrim_orig_ind, &b_t_fatjets_RebuildCamKt6MitruTrim_orig_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_assoc_ind", &t_fatjets_RebuildCamKt6MitruTrim_assoc_ind, &b_t_fatjets_RebuildCamKt6MitruTrim_assoc_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt6MitruTrim_truth_label", &t_fatjets_RebuildCamKt6MitruTrim_truth_label, &b_t_fatjets_RebuildCamKt6MitruTrim_truth_label);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_n", &t_fatjets_RebuildCamKt10MitruSplitFilt_n, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_n);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_pt", &t_fatjets_RebuildCamKt10MitruSplitFilt_pt, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_pt);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_eta", &t_fatjets_RebuildCamKt10MitruSplitFilt_eta, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_eta);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_phi", &t_fatjets_RebuildCamKt10MitruSplitFilt_phi, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_phi);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_m", &t_fatjets_RebuildCamKt10MitruSplitFilt_m, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_m);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT12", &t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT12, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT12);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT23", &t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT23, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_SPLIT23);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_Tau1", &t_fatjets_RebuildCamKt10MitruSplitFilt_Tau1, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_Tau1);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_Tau2", &t_fatjets_RebuildCamKt10MitruSplitFilt_Tau2, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_Tau2);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_Tau3", &t_fatjets_RebuildCamKt10MitruSplitFilt_Tau3, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_Tau3);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_MCclass", &t_fatjets_RebuildCamKt10MitruSplitFilt_MCclass, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_MCclass);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_orig_ind", &t_fatjets_RebuildCamKt10MitruSplitFilt_orig_ind, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_orig_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_assoc_ind", &t_fatjets_RebuildCamKt10MitruSplitFilt_assoc_ind, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_assoc_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruSplitFilt_truth_label", &t_fatjets_RebuildCamKt10MitruSplitFilt_truth_label, &b_t_fatjets_RebuildCamKt10MitruSplitFilt_truth_label);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_n", &t_fatjets_RebuildCamKt10MitruTrim_n, &b_t_fatjets_RebuildCamKt10MitruTrim_n);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_pt", &t_fatjets_RebuildCamKt10MitruTrim_pt, &b_t_fatjets_RebuildCamKt10MitruTrim_pt);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_eta", &t_fatjets_RebuildCamKt10MitruTrim_eta, &b_t_fatjets_RebuildCamKt10MitruTrim_eta);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_phi", &t_fatjets_RebuildCamKt10MitruTrim_phi, &b_t_fatjets_RebuildCamKt10MitruTrim_phi);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_m", &t_fatjets_RebuildCamKt10MitruTrim_m, &b_t_fatjets_RebuildCamKt10MitruTrim_m);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_SPLIT12", &t_fatjets_RebuildCamKt10MitruTrim_SPLIT12, &b_t_fatjets_RebuildCamKt10MitruTrim_SPLIT12);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_SPLIT23", &t_fatjets_RebuildCamKt10MitruTrim_SPLIT23, &b_t_fatjets_RebuildCamKt10MitruTrim_SPLIT23);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_Tau1", &t_fatjets_RebuildCamKt10MitruTrim_Tau1, &b_t_fatjets_RebuildCamKt10MitruTrim_Tau1);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_Tau2", &t_fatjets_RebuildCamKt10MitruTrim_Tau2, &b_t_fatjets_RebuildCamKt10MitruTrim_Tau2);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_Tau3", &t_fatjets_RebuildCamKt10MitruTrim_Tau3, &b_t_fatjets_RebuildCamKt10MitruTrim_Tau3);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_MCclass", &t_fatjets_RebuildCamKt10MitruTrim_MCclass, &b_t_fatjets_RebuildCamKt10MitruTrim_MCclass);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_orig_ind", &t_fatjets_RebuildCamKt10MitruTrim_orig_ind, &b_t_fatjets_RebuildCamKt10MitruTrim_orig_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_assoc_ind", &t_fatjets_RebuildCamKt10MitruTrim_assoc_ind, &b_t_fatjets_RebuildCamKt10MitruTrim_assoc_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildCamKt10MitruTrim_truth_label", &t_fatjets_RebuildCamKt10MitruTrim_truth_label, &b_t_fatjets_RebuildCamKt10MitruTrim_truth_label);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_n", &t_fatjets_RebuildAntiKt4Mitru_n, &b_t_fatjets_RebuildAntiKt4Mitru_n);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_pt", &t_fatjets_RebuildAntiKt4Mitru_pt, &b_t_fatjets_RebuildAntiKt4Mitru_pt);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_eta", &t_fatjets_RebuildAntiKt4Mitru_eta, &b_t_fatjets_RebuildAntiKt4Mitru_eta);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_phi", &t_fatjets_RebuildAntiKt4Mitru_phi, &b_t_fatjets_RebuildAntiKt4Mitru_phi);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_m", &t_fatjets_RebuildAntiKt4Mitru_m, &b_t_fatjets_RebuildAntiKt4Mitru_m);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_SPLIT12", &t_fatjets_RebuildAntiKt4Mitru_SPLIT12, &b_t_fatjets_RebuildAntiKt4Mitru_SPLIT12);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_SPLIT23", &t_fatjets_RebuildAntiKt4Mitru_SPLIT23, &b_t_fatjets_RebuildAntiKt4Mitru_SPLIT23);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_Tau1", &t_fatjets_RebuildAntiKt4Mitru_Tau1, &b_t_fatjets_RebuildAntiKt4Mitru_Tau1);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_Tau2", &t_fatjets_RebuildAntiKt4Mitru_Tau2, &b_t_fatjets_RebuildAntiKt4Mitru_Tau2);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_Tau3", &t_fatjets_RebuildAntiKt4Mitru_Tau3, &b_t_fatjets_RebuildAntiKt4Mitru_Tau3);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_MCclass", &t_fatjets_RebuildAntiKt4Mitru_MCclass, &b_t_fatjets_RebuildAntiKt4Mitru_MCclass);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_orig_ind", &t_fatjets_RebuildAntiKt4Mitru_orig_ind, &b_t_fatjets_RebuildAntiKt4Mitru_orig_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_assoc_ind", &t_fatjets_RebuildAntiKt4Mitru_assoc_ind, &b_t_fatjets_RebuildAntiKt4Mitru_assoc_ind);
   fChain->SetBranchAddress("t_fatjets_RebuildAntiKt4Mitru_truth_label", &t_fatjets_RebuildAntiKt4Mitru_truth_label, &b_t_fatjets_RebuildAntiKt4Mitru_truth_label);
   fChain->SetBranchAddress("t_mcevt_weight", &t_mcevt_weight, &b_t_mcevt_weight);
   fChain->SetBranchAddress("t_HF_class", &t_HF_class, &b_t_HF_class);
}



#endif // #ifdef Reader_cxx
