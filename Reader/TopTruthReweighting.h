#ifndef TopTruthReweighting_h
#define TopTruthReweighting_h

#include <iostream>

using namespace std;
class TopTruthReweighting {

 public:
  TopTruthReweighting();
  ~TopTruthReweighting();
 
  float GetDataReweight_Powheg(float top_pt, float ttbar_pt, int sys=0) ;
  float GetDataReweight_PowhegHerwig(float top_pt, float ttbar_pt) ;
  float GetTTbarPtWeight_Powheg(float pT, int sys) ;
  float GetSequentialTopPtWeight_Powheg(float pT, int sys) ;

 private:
 

};

#endif
  
